### pointy-poisson-statistic

A simple module that will depend on iDQ's codebase to discover triggers and will generate pointy-poisson statistics based on a stream of triggers. May be eventually incorporated into iDQ itself, but for now is written as a stand-alone module.

This is meant to be used as a stand-alone executable as part of the [Data Quality Report](https://git.ligo.org/detchar/data-quality-report).

Dependencies:

  * KleineWelle (`https://redoubt.ligo-wa.caltech.edu/svn/gds/trunk/Monitors/kleineWelle/`) 
  * html.py: (`pip install html==1.16`)
  * numpy


TO DO:

  * implement an actual log likelihood ration instead of just taking the ratio of the cumulative distribution
  * deal with "logpost" as 1/(1+likelihood) instead of loglike to keep in line with the resr of the conventions defined with logpvalue in min
  * write a dagwriter that only schedules kwm jobs (useful to have this as a stand alone without the pointy stuff on top
  * write timeseries dictionary thing returned by pipeline.analyze into an hdf5 file (and write a reading function for convenience). this way we can pick up the full results (roc, whatever) without having to rerun everything


NOTE: 

lognaivebayes should be Gamma distributed according to

```
    p(lognaivebayes) ~ exp(lognaivebayes) * (-lognaivebayes)**N
```

where 

```
    lognaivebayes = \sum\limits_{i=1}^{i=N} logpvalue_i
```

assuming all pvalue_i are independent and uniformly distributed within U[0,1]. This gives us a natural way to quantify the probability of observing lognaivebayes just like we have a probablity of observing the minimum pvalue_i where we did.

proof:

p(lognb) = \int\limits_0^1 \prod_i dq_i \delta(lognb = \sum_j logq_j)
         = \int_{-\infty}^0 \prod_i dlogq_i exp(logq_i) \delta(lognb = \sum_j logq_j)
         = exp(lognb) * (-lognb)**N \int\limits_0^\infty \prod_i du_i \delta(1 = \sum_j u_j) | u_j = logq_j / lognb
         = exp(lognb) * (-lognb)**N \int\limits_0^1 \prod_i du_i \delta(1 = \sum_j u_j)
         = exp(lognb) * (-lognb)**N * (surface area of a N-dim simplex)
         \propto exp(lognb) * (-lognb)**N

note, this is exponential in the tail, so at some point "p(lognb < X) \propto X" should be a reasonable approximation.
