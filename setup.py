#!/usr/bin/env python

__usage__ = "setpy.py command [--options]"
__description__ = "standard install script"
__author__ = "Reed Essick (reed.essick@ligo.org)"

#-------------------------------------------------

#from distutils.core import setup
from setuptools import setup

import glob

setup(
    name = 'pointy-poisson-statistic',
    version = '0.1',
    url = 'https://git.ligo.org/reed.essick/pointy-poisson-statistic',
    author = __author__,
    author_email = 'reed.essick@ligo.org',
    description = __description__,
#    liscence = 'MIT License',
    scripts = glob.glob('bin/*'),
    packages = [
        'pointy',
    ],
    data_files = [],
    package_data = {},
    requires = [],
)
