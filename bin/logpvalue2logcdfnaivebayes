#!/usr/bin/env python

__usage__ = "bin/logpvalue2logcdfnaivebayes [--options] chan.npy [chan.npy ...]"
__doc__ = "a script that computes the naive bayes statistic from a set of logpvalues from separate channels"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os
from optparse import OptionParser

import numpy as np

### non-standard libraries
from pointy import pointy
from pointy import utils
from pointy import plots

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('', '--plot', default=False, action='store_true',
    help='generate a plot comparing the observed distribution to the theoretical prediction')

parser.add_option('-o', '--output-dir', default='.', type='string')
parser.add_option('-t', '--tag', default='', type='string')
parser.add_option('', '--figtype', default=[], type='string',
    help='can be repeated. If not supplied, will produce a png')

opts, args = parser.parse_args()
assert args, 'please supply at least 1 input argument\n%s'%__usage__

if not os.path.exists(opts.output_dir):
    os.makedirs(opts.output_dir)

opts.tag = "_"+opts.tag

if not opts.figtype:
    opts.figtype = plots.DEFAULT_FIGTYPES

#-------------------------------------------------

lpvls = []
start = None
dur = None
for path in args:
    array, s, d = utils.load(path, verbose=opts.verbose) ### assume all start, dur are the same
    lpvl = array['logpvalue']

    if start is None:
        start = s
    else:
        assert start==s, 'start times disagree!'

    if dur is None:
        dur = d
    else:
        assert dur==d, 'durations disagree!'

    lpvls.append(lpvl)

if opts.verbose:
    print('computing logcdfnaivebayes')
lognb = pointy.lognaivebayes(lpvls)
N = len(lpvls)
logcdfnb = pointy.logcdfnaivebayes(lognb, N)
utils.save(logcdfnb, 'logcdfnaivebayes', start, dur, directory=opts.output_dir, tag=opts.tag, verbose=opts.verbose)

if opts.plot: ### generate an overlay against theoretical predictions
    if opts.verbose:
        print('plotting comparison to theoretical predictions')

    fig = plots.logcdfnb(logcdfnb, N)
    plots.save(fig, '%s/lognaivebayes-sanitycheck%s'%(opts.output_dir, opts.tag), figtypes=opts.figtype, verbose=opts.verbose)
    plots.close(fig)
