#!/usr/bin/env python

__usage__ = "kw2logpvalue [--options] gps channel [channel ...]"
__doc__ = "a script that computes the logpvalue for a particular channel at a particular time"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os
import numpy as np
from optparse import OptionParser

### non-standard libraries
from pointy import pointy
from pointy import utils

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')
parser.add_option('-V', '--Verbose', default=False, action='store_true')

parser.add_option('', '--kwm-rootdir', default=utils.DEFAULT_KWM_ROOTDIR, type='string')
parser.add_option('', '--kwm-basename', default=utils.DEFAULT_KWM_BASENAME, type='string')

parser.add_option('', '--rate-estimate-gpsstart', default=-np.infty, type='float',
    help='the beginning of time that should be used when windowing triggers. \
DEFAULT=-infty')
parser.add_option('', '--rate-estimate-gpsstop', default=+np.infty, type='float',
    help='the end of time that should be used when windowing triggers. \
DEFAULT=+infty')

parser.add_option('-w', '--window', default=pointy.DEFAULT_WINDOW, type='float',
    help='window used within rate estimate (triggers within gps+/-window). \
DEFAULT=%f'%pointy.DEFAULT_WINDOW)
parser.add_option('-f', '--frac-dur', default=pointy.DEFAULT_FRAC_DUR, type='float',
    help='the minimum allowed dt within computation of logpvalue based on this fraction of the trigger\'s duration. \
DEFAULT=%f'%pointy.DEFAULT_FRAC_DUR)
parser.add_option('-T', '--threshold', default=[], type='float', action='append',
    help='the significance threshold used in the analysis. Can be repeated to supply multiple threholds. \
DEFAULT will use every significance found in the observed triggers, which is hella expensive.')

parser.add_option('-o', '--output-dir', default='.', type='string')
parser.add_option('-t', '--tag', default='', type='string')

opts, args = parser.parse_args()
assert len(args)>=2, 'please supply at least 2 input arguments\n%s'%__usage__
gps = float(args[0])
channels = args[1:]

if not os.path.exists(opts.output_dir):
    os.makedirs(opts.output_dir)

opts.tag = "_"+opts.tag

opts.verbose |= opts.Verbose

#-------------------------------------------------

### reading in triggers
if opts.verbose:
    print('reading triggers')
triggers = utils.paths2KW(utils.basename2paths(gps-opts.window, gps+opts.window, opts.kwm_basename, directory=opts.kwm_rootdir), channels=channels) ### do I/O once

### iterate through channels, computing statistic for each channel
for channel in channels:
    if opts.verbose:
        print('filtering channel='+channel)

    times = triggers[channel]['time']
    triggers[channel] = triggers[channel][(opts.rate_estimate_gpsstart<=times)*(times<opts.rate_estimate_gpsstop)]

for channel, lpvl in pointy.logpvalue(
        gps,
        triggers,
        window=opts.window,
        frac_dur=opts.frac_dur,
        thresholds=opts.threshold,
        verbose=opts.Verbose,
    ).items():
    utils.save(lpvl, channel, gps, 0, directory=opts.output_dir, tag=opts.tag, verbose=opts.verbose)
