#!/usr/bin/env python

__usage__ = "logpvalue2stacked [--options] [chan-gps1.npy [chan-gps2.npy ...] chan-timeseries.npy,gps1 [chan-timeseries.npy,gps2 ...]]"
__doc__ = "a script that stackes logpvalues from multiple times. This is the same as logpvalue2naivebayes, but we check different things for sanity."
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os

import numpy as np

from optparse import OptionParser

### non-standard libraries
from pointy import pointy
from pointy import utils

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('--normalize', default=False, action='store_true',
    help='take the average of logpvl instead of just the sum')

parser.add_option('-o', '--output-dir', default='.', type='string')
parser.add_option('-t', '--tag', default='', type='string')

opts, args = parser.parse_args()
assert args, 'please supply at least 1 input argument\n%s'%__usage__

if not os.path.exists(opts.output_dir):
    os.makedirs(opts.output_dir)

opts.tag = "_"+opts.tag

#-------------------------------------------------

lpvls = []
for path in args:
    if ',' in path: ### treat this as a timeseries
        path, gps = path.split(',')
        gps = float(gps)
        array, s, d = utils.load(path, verbose=opts.verbose) ### assume all start, dur are the same
        lpvl = array['logpvalue']
        lpvl = np.interp(gps, np.arange(s, s+d, 1.*d/len(lpvl)), lpvl)
    else:
        array, s, d = utils.load(path, verbose=opts.verbose) ### assume all start, dur are the same
        lpvl = array['logpvalue']
    lpvls.append(lpvl)

if opts.verbose:
    print('computing stacked')
ans = pointy.lognaivebayes(lpvls)
if opts.normalize:
    if opts.verbose:
        print('normalizing stacked pvalues (%d observations)'%len(lpvls))
    ans /= 1.*len(lpvls)
utils.save(ans, 'stacked', None, 0, directory=opts.output_dir, tag=opts.tag, verbose=opts.verbose)
