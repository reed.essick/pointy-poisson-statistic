#!/usr/bin/env python

__usage__ = "timeseries2segments [--options] timeseries.npy [timeseries.npy ...]"
__doc__ = "read in a timeseries and generate segments based on a threshold"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import numpy as np

from optparse import OptionParser

### non-standard libraries
from pointy import utils

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__description__)

parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('', '--gps-range', default=None, nargs=2, type='float',
    help='the gps range to analyze (e.g.: --gps-range $start $stop). \
DEFAULT is to use the full timeseries; this let\'s you select a subset of that.')

parser.add_option('', '--threshold', default=[], type='float', action='append',
    help='generate segments whenever the timeseries is below this threshold. \
Can be repeated. \
DEFAULT=[]')

parser.add_option('-o', '--output-dir', default='.', type='string')
parser.add_option('-t', '--tag', default='', type='string')

opts, args = parser.parse_args()
assert args, 'please supply at least one input argument\n%s'%__usage__
assert thresholds, 'please supply at least one threshold\n%s'%__usage__

if not os.path.exists(opts.output_dir):
    os.makedirs(opts.output_dir)

if opts.tag:
    opts.tag = "_"+opts.tag

#-------------------------------------------------

for npy in args:
    if opts.verbose:
        print('reading timeseries from: '+npy)
        x, start, dur = utils.load(npy)

    try: ### try to pull out logpvalue, for backward-compatibility
        x = x['logpvalue']
    except ValueError:
        pass

    gps = np.arange(start, start+dur, 1.*dur/len(x))
    if opts.gps_range is not None:

        # figure out new start and end
        start = np.max(start, opts.gps_range[0])
        end = np.min(start+dur, opts.gps_range[1])
        dur = end - start

        # downselect timeseries
        truth = (start<=gps)*(gps<end)
        x = x[truth]
        gps = gps[truth]

    #--------------------

    if opts.verbose:
        print('generating segments')
    for threshold in opts.threshold:
        segs = utils.segments(gps, x, threshold)

        path = utils.savetxt(segs, 'segments', start, dur, directory=opts.output_dir, tag='_%.6f%s'%(threshold, opts.tag))
        if opts.verbose:
            print('wrote segments to: '+path)
