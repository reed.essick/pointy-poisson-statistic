#!/usr/bin/env python

__usage__ = "timeseries2scans [--options] timeseries.npy scan.cnf"
__doc__ = "read in a timeseries, find local minima, and schedule OmegaScans for each"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os
import numpy as np

from optparse import OptionParser

### non-standard libraries
from pointy import utils
from pointy.plots import DEFAULT_COLORMAP
from pointy import scans
from pointy.scans import DEFAULT_EXE, COMMAND_TEMPLATE

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('', '--gps-range', default=None, nargs=2, type='float', 
    help='the gps range to analyze (e.g.: --gps-range $start $stop). \
DEFAULT is to use the full timeseries; this let\'s you select a subset of that.')

parser.add_option('-w', '--cluster-window', default=utils.DEFAULT_CLUSTER_WINDOW, type='float',
    help='the window used to cluster local minima from within the timeseries. \
DEFAULT=%.3f'%utils.DEFAULT_CLUSTER_WINDOW)
parser.add_option('-T', '--minimum-threshold', default=utils.DEFAULT_MINIMUM_THRESHOLD, type='float',
    help='require mimima to correspond to values below this. \
DEFAULT=%.3e'%utils.DEFAULT_MINIMUM_THRESHOLD)

parser.add_option('-o', '--output-dir', default='.', type='string')

parser.add_option('', '--omegascan-exe', default=DEFAULT_EXE, type='string',
    help='DEFAULT='+DEFAULT_EXE)
parser.add_option('', '--colormap', default=DEFAULT_COLORMAP, type='string',
    help='DEFAULT='+DEFAULT_COLORMAP)

parser.add_option('-l', '--launch', default=False, action='store_true',
    help='actually launch the OmegaScan jobs')

opts, args = parser.parse_args()
assert len(args)==2, 'please supply exactly 2 input arguments\n%s'%__usage__
npy, cnf = args

if not os.path.exists(opts.output_dir):
    os.makedirs(opts.output_dir)

#-------------------------------------------------

if opts.verbose:
    print('reading timeseries from: '+npy)
x, start, dur = utils.load(npy)
try: ### try to pull out logpvalue, for backward-compatibility
    x = x['logpvalue']

except ValueError:
    pass

gps = np.arange(start, start+dur, 1.*dur/len(x))

if opts.gps_range is not None:

    # figure out new start and end
    start = np.max(start, opts.gps_range[0])
    end = np.min(start+dur, opts.gps_range[1])
    dur = end - start

    # downselect timeseries
    truth = (start<=gps)*(gps<end)
    x = x[truth]
    gps = gps[truth]

#------------------------

if opts.verbose:
    print('finding local minima')
minima = utils.local_minima(gps, x, cluster_window=opts.cluster_window, threshold=opts.minimum_threshold) ### times corresponding to the minima
if opts.verbose:
    print('found %d local minima:'%len(minima))
    for minimum in minima:
        print('    gps=%.6f'%minimum)

#------------------------

if opts.verbose:
    print('parsing required data types from: '+cnf)
frametypes = scans.cnf2frametypes(cnf)

#------------------------

### iterate over minima, scheduling a scan for each
framedir = os.path.join(opts.output_dir, 'frames')
if not os.path.exists(framedir):
    os.makedirs(framedir)

for minimum in minima:
    scans.scan(
        minimum,
        opts.omegascan_exe,
        cnf,
        frametypes=frametypes,
        output_dir=opts.output_dir,
        framedir=framedir,
        colormap=opts.colormap,
        launch=opts.launch,
        verbose=opts.verbose,
    )
