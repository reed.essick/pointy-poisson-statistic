__doc__ = "a module housing basic utilities"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os
import glob
import subprocess as sp

from collections import defaultdict
import numpy as np

#-------------------------------------------------

DEFAULT_KW_STRIDE = 32
DEFAULT_PRODUCTION_SIGNIFICANCE = 15.

DEFAULT_KWM_ROOTDIR = '.'
DEFAULT_KWM_BASENAME = 'KW_TRIGGERS'

DEFAULT_CLUSTER_WINDOW = 10.
DEFAULT_MINIMUM_THRESHOLD = -1.
DEFAULT_MINIMUM_FAP_THRESHOLD = 0.5

#------------------------

frqmap = {
        65536.0:[(8,128), (32,2048), (1024,4096), (2048,8192)],
        32768.0:[(8,128), (32,2048), (1024,4096), (2048,8192)],
        16384.0:[(8,128), (32,2048), (1024,4096), (2048,8192)],
         8192.0:[(8,128), (32,2048), (1024,4096), (2048,4096)],
         4096.0:[(8,128), (32,2048), (1024,2048)],
         2048.0:[(8,128), (32,1024)],
         1024.0:[(8,128), (32,512)],
          512.0:[(8,128), (32,256)],
          256.0:[(8,128)],
           16.0:[],
        }

KWCONFIG_HEADER = """stride %(stride).1f
basename %(basename)s
segname %(basename)s_SEGMENTS
significance %(significance).1f
threshold 3.0
decimateFactor -1"""

def kwbasename(frametype, tag):
    return "%s-KW_%s%s_TRIGGERS"%(frametype[0], frametype, '_'+tag if tag else '')

#-------------------------------------------------

def livetime(segs):
    return sum(e-s for s, e in segs)

#-------------------------------------------------

def output_path(channel, start, dur, directory='.', tag='', suffix='npy'):
    return '%s/%s%s-%s-%d.%s'%(directory, channel, tag, start, dur, suffix)

def path2channeltag_start_dur(path):
    basename = os.path.basename(path)
    fields = basename.split('.')[0].split('-') ### could be fragile...
    channeltag = '-'.join(fields[:-2])
    return channeltag, int(fields[-2]), int(fields[-1])

def save(x, channel, start, dur, directory='.', tag='', verbose=False):
    if start is not None: ### done so I can use this when stacking different times...
        start = '%d'%start

    path = output_path(channel, start, dur, directory=directory, tag=tag)
    if verbose:
        print('writing: '+path)
    np.save(path, x)
    return path

def savetxt(x, channel, start, dur, directory='.', tag='', verbose=False):
    if start is not None: ### done so I can use this when stacking different times...
        start = '%d'%start

    path = output_path(channel, start, dur, directory=directory, tag=tag, suffix='txt')
    if verbose:
        print('writing: '+path)
    np.savetxt(path, x)
    return path

def load(path, verbose=False):
    if verbose:
        print('loading: '+path)
    start, dur = path[:-4].split('-')[-2:]

    try: ### do this backflip in case I want to read in stacked values
        start = int(start)
    except ValueError:
        start = None

    dur = int(dur)
    return np.load(path), start, dur

def loadtxt(path, verbose=False):
    if verbose:
        print('loading: '+path)
    start, dur = path[:-4].split('-')[-2:]

    try: ### do this backflip in case I want to read in stacked values
        start = int(start)
    except ValueError:
        start = None

    dur = int(dur)
    return np.loadtxt(path), start, dur

#------------------------

def path2start_dur(path, suffix='gwf'):
    return [int(_) for _ in path[:-len(suffix)-1].split('-')[-2:]]

def path2channels(path):
    with open(path, 'r') as obj:
        channels = [line.strip().split()[0] for line in obj.readlines()] ### keep only the first column in case there are others
    return channels

def channels2path(channels, path):
    with open(path, 'w') as obj:
        for channel in channels:
            print >> obj, channel

def frametype2chanlist(start, stop, frametype, firstonly=True):
    """
    WARNING: will iterate through every frame found in this time period and take the super-set. That could be a lot of I/O...
    """
    inlist = frametype2inlist(start, stop, 0, frametype).split()
    if firstonly:
        inlist = inlist[:1]
    chanlist = set()
    for frame in inlist:
        for channel in frame2chanlist(frame):
            chanlist.add(channel)

    # some sorting to make it pretty
    chanlist = list(chanlist)
    chanlist.sort(key=lambda l: l[0]) # alphabetically
    chanlist.sort(key=lambda l: int(l[1])) # by sample rate
    return chanlist

def frame2chanlist(path):
    stdout, stderr = sp.Popen(['FrChannels', path], stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    if not stdout:
        raise ValueError('could not find any channels for frame=%s\n%s\n%s'%(path, stdout, stderr))
    chanlist = [line.strip().split() for line in stdout.strip().split('\n')]
    return [(chan, int(freq)) for chan, freq in chanlist]

def path2chanlist(path):
    with open(path, 'r') as obj:
        chanlist = [line.strip().split() for line in obj.readlines()]
    return [(chan, int(freq)) for chan, freq in chanlist]

def chanlist2path(chanlist, path):
    with open(path, 'w') as obj:
        for chan, freq in chanlist:
            print >> obj, '%s %d'%(chan, freq)

def path2times(path):
    return np.loadtxt(path)

def times2path(times, path):
    np.savetxt(path, times)

#------------------------

def basename2paths(start, end, basename, directory='.', verbose=False):
    """
    discover KW trigger files
    """
    paths = []
    if verbose:
        print('globbing in '+directory)
    for basedir in sorted(glob.glob('%s/%s-*'%(directory, basename))):
        if verbose:
            print('searching '+basedir)
        s = int(basedir.split('-')[-1])*1e5
        if (s<end) and (s+1e5>=start): ### there is some overlap
            for path in sorted(glob.glob('%s/%s-*.trg'%(basedir, basename))):
                S, D = [int(_) for _ in path[:-4].split('-')[-2:]]
                if (S<end) and (S+D>=start): ### again, there is some overlap
                    if verbose:
                        print('accepted '+path)
                    paths.append(path)
                elif verbose:
                    print('rejected '+path)
    return paths

DTYPE=[(col, 'float') for col in 'start_time stop_time time frequency unnormalized_energy normalized_energy chisqdof significance'.split() + ['duration']]
START_IND = 0
STOP_IND = 1
def path2KW(path, channels=None):
    result = defaultdict(list)
    donotlook4channels = channels is None
    with open(path, 'r') as obj:
        obj.readline() ### skip the first line, which is just header information we've already hard-coded...
        for line in obj: ### iterate through the rest of the lines
            fields = line.strip().split()
            chan = fields[-1]
            if donotlook4channels or (chan in channels):
                fields = tuple(float(f) for f in fields[:-1])
                result[chan].append( fields+(fields[STOP_IND]-fields[START_IND],) )

    for chan, trgs in result.items():
        result[chan] = np.array(trgs, dtype=DTYPE)

    return result

def paths2KW(paths, channels=None, verbose=False):
    assert paths, 'please supply at least 1 path'
    if isinstance(paths, str):
        return path2KW(paths)

    result = path2KW(paths[0], channels=channels)
    for path in paths[1:]:
        if verbose:
            print('    '+path)
        chans = path2KW(path)
        for key, val in path2KW(path, channels=channels).items():
            if result.has_key(key):
                result[key] = np.concatenate((result[key], val))
            else:
                result[key] = val

    if channels is not None:
        for channel in channels:
            if not result.has_key(channel):
                result[channel] = np.array([], dtype=DTYPE) ### FIXME: do I need to cast this as a structured array?

    return result

#------------------------

def chanlist2channels(chanlist):
    channels = []
    for chan, freq in chanlist:
        for minF, maxF in frqmap[freq]:
            channels.append( "%s_%d_%d"%(chan.replace(':','_'), minF, maxF) )
    return channels

def chanlist2KWconfig(chanlist, observatory, basename, stride=DEFAULT_KW_STRIDE, significance=DEFAULT_PRODUCTION_SIGNIFICANCE):
    kwconfig = KWCONFIG_HEADER%{\
        'stride':stride,
        'observatory':observatory,
        'basename':basename,
        'significance':significance,
    }
    for chan, freq in chanlist:
        freq = int(freq)
        for minF, maxF in frqmap[freq]:
            kwconfig += "\nchannel %s %d %d"%(chan, minF, maxF)
    return kwconfig

def frametype2inlist(start, stop, stride, frametype, verbose=False):
    ### add some buffer to be safe
    start = start - stride ### KW will skip the first stride
    if stride!=0: ### handle this as a special case
        if (start%stride):
            start -= stride ### bump this back one more so we know we have full coverage

        if stop%stride: ### include enough data to cover the final stride
            stop += stride

    ### define command and do it
    cmd = "gw_data_find -o %s --type %s -u file -s %d -e %d"%(frametype[0], frametype, start, stop)
    if verbose:
        print(cmd)
    return sp.Popen(cmd.split(), stdout=sp.PIPE).communicate()[0].replace("file://localhost","")

def inlist2trgs(config, inlist, directory='.', tag='KW', verbose=False):
    cmd = "kleineWelleM %s -inlist %s"%(config, inlist)
    out = "%s/%s.out"%(directory, tag)
    err = "%s/%s.err"%(directory, tag)

    if verbose:
        print( "    cd %s"%directory )
        print("    "+cmd)
        print("    out : "+out)
        print("    err : "+err)

    out_obj = open(out, "w")
    err_obj = open(err, "w")

    cwd = os.getcwd()
    os.chdir(directory)
    proc = sp.Popen(cmd.split(), stdout=out_obj, stderr=err_obj)
    out_obj.close()
    err_obj.close()
    proc.wait()

    if verbose:
        print("    cd "+cwd)
    os.chdir(cwd)

def makeKWtrgs(start, stop, stride, frametype, chanlist_path=None, directory='.', tag='', verbose=False):
    """
    a one-stop-shop for generating KW triggers
    """
    basename = kwbasename(frametype, tag)
    dur = stop - start

    ### go grab data
    inlist_path = "%s/frames_%s-%d-%d.txt"%(directory, basename, start, dur)
    if verbose:
        print('writing inlist: '+inlist_path)
    with open(inlist_path, "w") as obj:
        inlist = frametype2inlist(start, stop, stride, frametype)
        obj.write(inlist)

    ### go grab chanlist
    if chanlist_path is None:
        frame = inlist.split()[0]
        if verbose:
            print('reading chanlist from frame: '+frame)
        chanlist = frame2chanlist(frame)

    else:
        if verbose:
            print('reading chanlist from: '+chanlist_path)
        chanlist = path2chanlist(chanlist_path)

    ### compute KW triggers for these channels
    kwconfig_path = "%s/KW_%s-%d-%d.cnf"%(directory, basename, start, dur)
    if verbose:
        print('writing config: '+kwconfig_path)
    with open(kwconfig_path, 'w') as obj:
        obj.write(chanlist2KWconfig(chanlist, frametype[0], basename=basename, stride=stride))

    ### launch KW trigger generation
    inlist2trgs(kwconfig_path, inlist_path, directory=directory, tag=basename, verbose=verbose)

def filter_kw(triggers, start, stop, signif):
    for channel in triggers.keys():
        trgs = triggers[channel]
        trgs = trgs[(start<=trgs['time'])*(trgs['time']<stop)] ### downselect based on coincidence window
        trgs = trgs[signif<=trgs['significance']]                   ### downselect based on significance
        triggers[channel] = trgs
    return triggers

#------------------------

def local_minima(x, y, cluster_window=DEFAULT_CLUSTER_WINDOW, threshold=DEFAULT_MINIMUM_THRESHOLD):
    '''
    find the lcoal minima of y and then cluster them with cluster_window (defines differences in x) that are at least as small as threshold
    returns the values of x corresponding to the minima
    '''
    ### find args of local minima
    N = len(y)
    positive_slope = y[1:]>=y[:-1]
    negative_slope = y[1:]<=y[:-1]

    args = []

    if positive_slope[0]: ### the starting boundary is a minimum
        args.append(0)

    args += list(np.arange(N-2)[negative_slope[:-1]*positive_slope[1:]]+1) ### find the relative local minima

    if negative_slope[-1]:
        args.append(N-1) ### the ending boundary is a minimum

    xmin = x[args] ### the x values of the local minima
    ymin = y[args]

    ### keep only the ones that are below threshold
    truth = ymin <= threshold
    xmin = xmin[truth]
    ymin = ymin[truth]

    ### cluster local minima, then select the smallest within each cluster
    clustered_minima = []
    while len(ymin):
        ### find the mimimum of remaining times, add to clustered minima
        arg = np.argmin(ymin)
        X = xmin[arg]
        clustered_minima.append(X)

        ### remove all times corresponding to this cluster
        t = np.abs(X-xmin) > cluster_window
        xmin = xmin[t]
        ymin = ymin[t]

    return sorted(clustered_minima)

def cluster(*args, **kwargs):
    """cluster sets of times together based on cluster_window (passed as a kwarg)
    """
    cluster_window = kwargs.get('window', DEFAULT_CLUSTER_WINDOW)

    ### map everything into a big list of (times, index) pairs, which is easy to cluster
    stuff = []
    N = len(args)
    for ind, arg in enumerate(args):
        for gps in arg:
            stuff.append( (gps, ind) )

    # cluster all this things in stuff so they're grouped
    stuff.sort(key=lambda x:x[0]) ### sort the times
    flat_groups = []
    previous = -np.infty
    for tup in stuff:
        if tup[0]-previous > cluster_window: ### start a new cluster
            group = [tup]
            flat_groups.append(group)
        else:
            group.append(tup)
        previous = tup[0]

    ### format the data
    final_groups = []
    for group in flat_groups:
        new = [[] for _ in range(N)]
        for gps, ind in group:
            new[ind].append(gps)
        final_groups.append(new)

    return final_groups

def segments(x, y, threshold):
    N = len(x)
    assert len(y)==N, 'x and y must have the same length'

    change = np.arange(N)[np.diff(y<=threshold)!=0]
    xATchange = list(x[change] + (threshold-y[change])*(x[change+1]-x[change])/(y[change+1]-y[change]))

    if y[0]<=threshold:
        xATchange.insert(0, x[0])
    if y[-1]<=threshold:
        xATchange.append(x[-1])
    assert len(xATchange)%2==0, 'must have an even number!'

    return np.array(zip(xATchange[::2], xATchange[1::2]))

#------------------------

def str2obj( val, warn=False ):
    '''
    converts strings to a variety of objects. 
    If the string is not understood, it is simply kept as a string unless warn=True. Then, a ValueError is raised.
    '''
    val = val.strip()
    if (val[0]=="'") and val.endswith("'"): ### normal string
        val = val.strip("'")

    elif (val[0]=='"') and val.endswith('"'): ### normal string
        val = val.strip("'")

    elif (val[0]=='[') and val.endswith(']'): ### a list of things
        val = [str2obj( v ) for v in val[1:-1].split()] ### assumes space-delimited

    else: ### probably an int or a float
        try:
            val = int(val)
        except:
            try:
                val = float(val)
            except:
                if warn:
                    raise ValueError('could not parse val=%s'%val)
    return val
