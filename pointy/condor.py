__description__ = "a module that houses my condor functionality for writing subs and dags"
__author__ = "reed.essick@ligo.org"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os
import getpass
from distutils.spawn import find_executable

import random
import numpy as np

### non-standard libraries
from . import utils
from . import pointy

#-------------------------------------------------

DEFAULT_RETRY = 1
DEFAULT_KWM_UNIVERSE = 'local'
DEFAULT_LOGPVALUE_UNIVERSE = 'vanilla'
DEFAULT_LOGNAIVEBAYES_UNIVERSE = 'vanilla'

DEFAULT_MAX_DURATION = 512
DEFAULT_MAX_CHANNELS = 100

ALPHA_NUMERIC = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z'.split()

#-------------------------------------------------

def gen_jobid(size=6):
    return ''.join(random.choice(ALPHA_NUMERIC) for _ in xrange(size))

#-------------------------------------------------

def kwm(
        accounting_group,
        gpsstart,
        gpsstop,
        frametype,
        chanlist=None,
        stride=utils.DEFAULT_KW_STRIDE,
        max_duration=DEFAULT_MAX_DURATION,
        output_dir='.',
        tag='',
        verbose=False,
        universe=DEFAULT_KWM_UNIVERSE,
        accounting_group_user=getpass.getuser(),
    ):
    path = kwm_sub_path(output_dir=output_dir, tag=tag)
    with open(path, 'w') as obj:
        obj.write(kwm_sub_str(
            accounting_group,
            frametype,
            chanlist=chanlist,
            stride=stride,
            output_dir=output_dir,
            tag=tag,
            verbose=verbose,
            universe=universe,
            accounting_group_user=accounting_group_user,
        ))
    grouped = kwm_var(gpsstart, gpsstop, frametype, stride=stride, max_duration=max_duration, output_dir=output_dir, tag=tag)
    return path, grouped

def kwm_sub_path(output_dir=',', tag=''):
    return os.path.join(output_dir, 'frametype2kw%s.sub'%('_'+tag if tag else ''))

def kwm_sub_str(
        accounting_group,
        frametype,
        chanlist=None,
        stride=utils.DEFAULT_KW_STRIDE,
        output_dir='.',
        tag='',
        verbose=False,
        universe=DEFAULT_KWM_UNIVERSE,
        accounting_group_user=getpass.getuser(),
    ):
    """
    frametype2kw --chanlist chanlist --stride stride --output-dir output_dir --tag tag --verbose frametype gpsstart gpsstop
    """
    return '''\
universe = %(universe)s
executable = %(executable)s
arguments = "$(START) $(STOP) %(frametype)s %(chanlist)s --stride %(stride)d --output-dir %(output_dir)s %(tag)s %(verbose)s"
log = %(output_dir)s/frametype2kw-%(frametype)s%(filetag)s-$(START)-$(DUR).log
output = %(output_dir)s/frametype2kw-%(frametype)s%(filetag)s-$(START)-$(DUR).out
error = %(output_dir)s/frametype2kw-%(frametype)s%(filetag)s-$(START)-$(DUR).err
getenv = True
accounting_group = %(accounting_group)s
accounting_group_user = %(accounting_group_user)s
queue 1'''%{
        'universe':universe,
        'executable':find_executable('frametype2kw'),
        'output_dir':output_dir,
        'tag':'--tag '+tag if tag else '',
        'filetag':"_"+tag,
        'verbose':'--verbose' if verbose else '',
        'stride':stride,
        'chanlist':'--chanlist '+chanlist if (chanlist is not None) else '',
        'frametype':frametype,
        'accounting_group':accounting_group,
        'accounting_group_user':accounting_group_user,
    }

def kwm_var(gpsstart, gpsstop, frametype, stride=utils.DEFAULT_KW_STRIDE, max_duration=DEFAULT_MAX_DURATION, output_dir='.', tag=''):
    inlist = utils.frametype2inlist(gpsstart, gpsstop, stride, frametype).strip().split('\n') ### list of frames
    start_durs = [utils.path2start_dur(frame, suffix='gwf') for frame in inlist]

    start = min([start for start, _ in start_durs])
    stop = max([s+d for s, d in start_durs])

    ### NOTE: because of the way klieneWelleM reads in frames, we basically have to have one big job to avoid repeated work...
    var = ['START="%d" STOP="%d" DUR="%d"'%(start, stop, stop-start)]

    ### now figure out the output paths
    basename = utils.kwbasename(frametype, tag)

    start = (start/stride)*stride ### integer division
    start += stride ### kleineWelleM will skip the first stride determinisitically
    out = []
    while start < stop:
        start1e5 = start/100000
        path = "%s/%s-%d/%s-%d-%d.trg"%(output_dir, basename, start1e5, basename, start, stride)
        out.append(out)
        start += stride
    out = [out] ### wrap this accordingly so len(var) == len(out)
    return [(var, out)] ### group this into sets that cover separate ranges of time

#------------------------

def logpvalue(
        accounting_group,
        gps_list,
        rate_estimate_gpsstart,
        rate_estimate_gpsstop,
        channels,
        kwm_rootdir=utils.DEFAULT_KWM_ROOTDIR,
        kwm_basename=utils.DEFAULT_KWM_BASENAME,
        max_channels=DEFAULT_MAX_CHANNELS,
        window=pointy.DEFAULT_WINDOW,
        frac_dur=pointy.DEFAULT_FRAC_DUR,
        thresholds=pointy.DEFAULT_THRESHOLDS,
        output_dir='.',
        tag='',
        verbose=False,
        universe=DEFAULT_LOGPVALUE_UNIVERSE,
        accounting_group_user=getpass.getuser(),
    ):
    path = logpvalue_sub_path(output_dir=output_dir, tag=tag)
    with open(path, 'w') as obj:
        obj.write(logpvalue_sub_str(
            accounting_group,
            kwm_rootdir=kwm_rootdir,
            kwm_basename=kwm_basename,
            window=window,
            frac_dur=frac_dur,
            thresholds=thresholds,
            output_dir=output_dir,
            tag=tag,
            verbose=verbose,
            universe=universe,
            accounting_group_user=accounting_group_user,
        ))
    grouped = logpvalue_var(gps_list, channels, rate_estimate_gpsstart=rate_estimate_gpsstart, rate_estimate_gpsstop=rate_estimate_gpsstop, window=window, max_channels=max_channels)
    return path, grouped

def logpvalue_sub_path(output_dir='.', tag=''):
    return os.path.join(output_dir, 'kw2logpvalue%s.sub'%('_'+tag if tag else ''))

def logpvalue_sub_str(
        accounting_group,
        kwm_rootdir=utils.DEFAULT_KWM_ROOTDIR,
        kwm_basename=utils.DEFAULT_KWM_BASENAME,
        window=pointy.DEFAULT_WINDOW,
        frac_dur=pointy.DEFAULT_FRAC_DUR,
        thresholds=pointy.DEFAULT_THRESHOLDS,
        output_dir='.',
        tag='',
        verbose=False,
        universe=DEFAULT_LOGPVALUE_UNIVERSE,
        accounting_group_user=getpass.getuser()
    ):
    """
    kw2logpvalue --kwm-rootdir kwm_rootdir --kwm-basename kwm_basename, --rate-estimate-gpsstart rate_estimate_gpsstart --rate-estimate-gpsstop rate-estiamte-gpsstop --window --frac-dur [--threshold t for t in thresholds] --output-dir output_dir --tag tag --verbose gps [chan for chan in channels]
    """
    return '''\
universe = %(universe)s
executable = %(executable)s
arguments = "$(GPS) $(CHANNEL) --kwm-rootdir %(kwm_rootdir)s --kwm-basename %(kwm_basename)s --rate-estimate-gpsstart $(RATE_START) --rate-estimate-gpsstop $(RATE_STOP) --window %(window)f --frac-dur %(frac_dur)f %(thresholds)s --output-dir %(output_dir)s %(tag)s %(verbose)s"
log = %(output_dir)s/kw2logpvalue%(filetag)s-$(CHUNK)-$(GPS).log
output = %(output_dir)s/kw2logpvalue%(filetag)s-$(CHUNK)-$(GPS).out
error = %(output_dir)s/kw2logpvalue%(filetag)s-$(CHUNK)-$(GPS).err
getenv = True
accounting_group = %(accounting_group)s
accounting_group_user = %(accounting_group_user)s
queue 1'''%{
        'universe':universe,
        'executable':find_executable('kw2logpvalue'),
        'output_dir':output_dir,
        'tag':'--tag '+tag if tag else '',
        'filetag':"_"+tag,
        'verbose':'--verbose' if verbose else '',
        'kwm_rootdir':kwm_rootdir,
        'kwm_basename':kwm_basename,
        'window':window,
        'frac_dur':frac_dur,
        'thresholds':' '.join(['--threshold %f'%thr for thr in thresholds]) if (thresholds is not None) else '',
        'accounting_group':accounting_group,
        'accounting_group_user':accounting_group_user,
    }

def logpvalue_var(gps_list, channels, rate_estimate_gpsstart=-np.infty, rate_estimate_gpsstop=+np.infty, window=pointy.DEFAULT_WINDOW, max_channels=DEFAULT_MAX_CHANNELS, output_dir='.', tag=''):
    if isinstance(gps_list, (int, float)):
        gps_list = [gps_list]

    N = len(channels)
    filetag = '_'+tag if tag else ''
    grouped = []
    for gps in gps_list:
        var = []
        out = []
        i = 0
        rate_start = max(rate_estimate_gpsstart, gps-window)
        rate_stop = min(rate_estimate_gpsstop, gps+window)
        while i < N:
            var.append( 'GPS="%.9f" CHANNEL="%s" CHUNK="%d" RATE_START="%d" RATE_STOP="%d"'%(gps, ' '.join(channels[i:i+max_channels]), i, rate_start, rate_stop) )
            these = []
            for channel in channels[i:i+max_channels]:
                these.append(utils.output_path(channel, gps, 0, directory=output_dir, tag=filetag))
            out.append(these)
            i += max_channels
        grouped.append((var, out))
    return grouped

#------------------------

def logpvalue_timeseries(
        accounting_group,
        gpsstart,
        gpsstop,
        rate_estimate_gpsstart,
        rate_estimate_gpsstop,
        channels,
        kwm_rootdir=utils.DEFAULT_KWM_ROOTDIR,
        kwm_basename=utils.DEFAULT_KWM_BASENAME,
        srate=pointy.DEFAULT_SRATE,
        window=pointy.DEFAULT_WINDOW,
        frac_dur=pointy.DEFAULT_FRAC_DUR,
        thresholds=pointy.DEFAULT_THRESHOLDS,
        output_dir='.',
        tag='',
        verbose=False,
        max_duration=DEFAULT_MAX_DURATION,
        max_channels=DEFAULT_MAX_CHANNELS,
        universe=DEFAULT_LOGPVALUE_UNIVERSE,
        accounting_group_user=getpass.getuser(),
    ):
    path = logpvalue_timeseries_sub_path(output_dir=output_dir, tag=tag)
    with open(path, 'w') as obj:
        obj.write(logpvalue_timeseries_sub_str(
            accounting_group,
            kwm_rootdir=kwm_rootdir,
            kwm_basename=kwm_basename,
            srate=srate,
            window=window,
            frac_dur=frac_dur,
            thresholds=thresholds,
            output_dir=output_dir,
            tag=tag,
            verbose=verbose,
            universe=universe,
            accounting_group_user=accounting_group_user,
        ))
    grouped = logpvalue_timeseries_var(gpsstart, gpsstop, channels, rate_estimate_gpsstart=rate_estimate_gpsstart, rate_estimate_gpsstop=rate_estimate_gpsstop, window=window, max_duration=max_duration, max_channels=max_channels, output_dir=output_dir, tag=tag)
    return path, grouped

def logpvalue_timeseries_sub_path(output_dir='.', tag=''):
    return os.path.join(output_dir, 'kw2logpvalue_timeseries%s.sub'%('_'+tag if tag else ''))

def logpvalue_timeseries_sub_str(
        accounting_group,
        kwm_rootdir=utils.DEFAULT_KWM_ROOTDIR,
        kwm_basename=utils.DEFAULT_KWM_BASENAME,
        srate=pointy.DEFAULT_SRATE,
        window=pointy.DEFAULT_WINDOW,
        frac_dur=pointy.DEFAULT_FRAC_DUR,
        thresholds=pointy.DEFAULT_THRESHOLDS,
        output_dir='.',
        tag='',
        verbose=False,
        universe=DEFAULT_LOGPVALUE_UNIVERSE,
        accounting_group_user=getpass.getuser(),
    ):
    """
    kw2logpvalue_timeseries --kwm-rootdir kwm_rootdir --kwm-basename kwm_basename, --rate-estimate-gpsstart rate_estimate_gpsstart --rate-estimate-gpsstop rate-estiamte-gpsstop --srate srate --window --frac-dur [--threshold t for t in thresholds] --output-dir output_dir --tag tag --verbose gpsstart gpsstop [chan for chan in channels]
    """
    return '''\
universe = %(universe)s
executable = %(executable)s
arguments = "$(START) $(STOP) $(CHANNEL) --kwm-rootdir %(kwm_rootdir)s --kwm-basename %(kwm_basename)s --rate-estimate-gpsstart $(RATE_START) --rate-estimate-gpsstop $(RATE_STOP) --srate %(srate)f --window %(window)f --frac-dur %(frac_dur)f %(thresholds)s --output-dir %(output_dir)s %(tag)s %(verbose)s"
log = %(output_dir)s/kw2logpvalue_timeseries%(filetag)s-$(CHUNK)-$(START)-$(DUR).log
output = %(output_dir)s/kw2logpvalue_timeseries%(filetag)s-$(CHUNK)-$(START)-$(DUR).out
error = %(output_dir)s/kw2logpvalue_timeseries%(filetag)s-$(CHUNK)-$(START)-$(DUR).err
getenv = True
accounting_group = %(accounting_group)s
accounting_group_user = %(accounting_group_user)s
queue 1'''%{
        'universe':universe,
        'executable':find_executable('kw2logpvalue_timeseries'),
        'output_dir':output_dir,
        'tag':'--tag '+tag if tag else '',
        'filetag':"_"+tag,
        'verbose':'--verbose' if verbose else '',
        'kwm_rootdir':kwm_rootdir,
        'kwm_basename':kwm_basename,
        'srate':srate,
        'window':window,
        'frac_dur':frac_dur,
        'thresholds':' '.join(['--threshold %f'%thr for thr in thresholds]) if (thresholds is not None) else '',
        'accounting_group':accounting_group,
        'accounting_group_user':accounting_group_user,
    }

def logpvalue_timeseries_var(gpsstart, gpsstop, channels, rate_estimate_gpsstart=-np.infty, rate_estimate_gpsstop=+np.infty, window=pointy.DEFAULT_WINDOW, max_duration=DEFAULT_MAX_DURATION, max_channels=DEFAULT_MAX_CHANNELS, output_dir='.', tag=''):
    N = len(channels)
    filetag = '_'+tag if tag else ''
    grouped = []
    while gpsstart < gpsstop:
        var = []
        out = []
        stop = min(gpsstart+max_duration, gpsstop)
        rate_start = max(rate_estimate_gpsstart, gpsstart-window)
        rate_stop = min(rate_estimate_gpsstop, stop+window)
        i = 0
        while i < N:
            var.append( 'START="%d" STOP="%d" DUR="%d" CHANNEL="%s" CHUNK="%d" RATE_START="%d" RATE_STOP="%d"'%(gpsstart, stop, stop-gpsstart, ' '.join(channels[i:i+max_channels]), i, rate_start, rate_stop) )
            these = []
            for channel in channels[i:i+max_channels]:
                these.append(utils.output_path(channel, gpsstart, stop-gpsstart, directory=output_dir, tag=filetag))
            out.append(these)
            i += max_channels
        gpsstart += max_duration
        grouped.append((var, out))
    return grouped

#------------------------

def lognaivebayes(
        accounting_group,
        gps_list,
        channels,
        output_dir='.',
        tag='',
        verbose=False,
        universe=DEFAULT_LOGNAIVEBAYES_UNIVERSE,
        accounting_group_user=getpass.getuser(),
    ):
    path = lognaivebayes_sub_path(output_dir=output_dir, tag=tag)
    with open(path, 'w') as obj:
        obj.write(lognaivebayes_sub_str(
            accounting_group,
            output_dir=output_dir,
            tag=tag,
            verbose=verbose,
            accounting_group_user=accounting_group_user,
            universe=universe,
        ))
    grouped = lognaivebayes_var(gps_list, channels, output_dir=output_dir, tag=tag)
    return path, grouped

def lognaivebayes_sub_path(output_dir='.', tag=''):
    return os.path.join(output_dir, 'logpvalue2lognaivebayes%s.sub'%('_'+tag if tag else ''))

def lognaivebayes_sub_str(accounting_group, output_dir='.', tag='', verbose=False, accounting_group_user=getpass.getuser(), universe=DEFAULT_LOGNAIVEBAYES_UNIVERSE):
    """
    logpvalue2lognaivebayes --output-dir output_dir --tag tag --verbose [path for path in paths]
    """
    return '''\
universe = %(universe)s
executable = %(executable)s
arguments = "$(PATHS) --output-dir %(output_dir)s %(tag)s %(verbose)s"
log = %(output_dir)s/logpvalue2lognaivebayes%(filetag)s.log
output = %(output_dir)s/logpvalue2lognaivebayes%(filetag)s.out
error = %(output_dir)s/logpvalue2lognaivebayes%(filetag)s.err
getenv = True
accounting_group = %(accounting_group)s
accounting_group_user = %(accounting_group_user)s
queue 1'''%{
        'universe':universe,
        'executable':find_executable('logpvalue2lognaivebayes'),
        'output_dir':output_dir,
        'tag':'--tag '+tag if tag else '',
        'filetag':"_"+tag,
        'verbose':'--verbose' if verbose else '',
        'accounting_group':accounting_group,
        'accounting_group_user':accounting_group_user,
    }

def lognaivebayes_var(gps_list, channels, output_dir='.', tag=''):
    if isinstance(gps_list, (int, float)):
        gps_list = [gps_list]
    grouped = []
    filetag = '_'+tag if tag else ''
    for gps in gps_list:
        
        paths = [utils.output_path(channel, int(gps), 0, directory=output_dir, tag=filetag) for channel in channels]
        var = ['PATHS="%s"'%' '.join(paths)]
        out = [[utils.output_path('lognaivebayes', int(gps), 0, directory=output_dir, tag=filetag)]]
        grouped.append((var, out))
    return grouped

#------------------------

def lognaivebayes_timeseries(
        accounting_group,
        gpsstart,
        gpsstop,
        channels,
        max_duration=DEFAULT_MAX_DURATION,
        output_dir='.',
        tag='',
        verbose=False,
        universe=DEFAULT_LOGNAIVEBAYES_UNIVERSE,
        accounting_group_user=getpass.getuser(),
    ):
    path = lognaivebayes_timeseries_sub_path(output_dir=output_dir, tag=tag)
    with open(path, 'w') as obj:
        obj.write(lognaivebayes_timeseries_sub_str(
            accounting_group,
            output_dir=output_dir,
            tag=tag,
            verbose=verbose,
            accounting_group_user=accounting_group_user,
            universe=universe,
        ))
    grouped = lognaivebayes_timeseries_var(gpsstart, gpsstop, channels, max_duration=max_duration, output_dir=output_dir, tag=tag)
    return path, grouped

def lognaivebayes_timeseries_sub_path(output_dir='.', tag=''):
    return lognaivebayes_sub_path(output_dir=output_dir, tag=tag)

def lognaivebayes_timeseries_sub_str(accounting_group, output_dir='.', tag='', verbose=False, accounting_group_user=getpass.getuser(), universe=DEFAULT_LOGNAIVEBAYES_UNIVERSE):
    """
    logpvalue2lognaivebayes --output-dir output_dir --tag tag --verbose [path for path in paths]
    """
    return lognaivebayes_sub_str(accounting_group, output_dir=output_dir, tag=tag, verbose=verbose, accounting_group_user=accounting_group_user, universe=universe)

def lognaivebayes_timeseries_var(gpsstart, gpsstop, channels, max_duration=DEFAULT_MAX_DURATION, output_dir='.', tag=''):
    filetag = '_'+tag if tag else ''
    grouped = []
    while gpsstart < gpsstop:
        stop = min(gpsstart+max_duration, gpsstop)

        paths = [utils.output_path(channel, gpsstart, stop-gpsstart, directory=output_dir, tag=filetag) for channel in channels]
        var = ['PATHS="%s"'%' '.join(paths)]
        out = [[utils.output_path('lognaivebayes', gpsstart, stop-gpsstart, directory=output_dir, tag=filetag)]]
        grouped.append((var, out))
        gpsstart += max_duration

    return grouped

#-------------------------------------------------

TEMPLATE='''\
JOB %(jobid)s %(sub)s
VARS %(jobid)s %(var)s
RETRY %(jobid)s %(retry)d
'''

# this is "TEMPLATE" backwards and not some crazy typo
ETALPMET = '''\
PARENT %(parent)s CHILD %(child)s
'''

def dag_str(jobs, parent_child):
    result = ""
    for jobid, (sub, var, retry) in jobs.items():
        result += TEMPLATE%{'jobid':jobid, 'sub':sub, 'var':var, 'retry':retry}
    for parent, children in parent_child.items():
        result += ETALPMET%{'parent':parent, 'child':' '.join(children)}
    return result
