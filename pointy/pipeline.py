__doc__ = "a module housing basic utilities"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os
import logging
import numpy as np

### non-standard libraries
from . import utils
from . import pointy

#-------------------------------------------------

DEFAULT_TARGET_SIGNIFICANCE = 35
DEFAULT_ACTIVE_SIGNIFICANCE = -np.infty ### signif must be larger than this to be declared active

DEFAULT_COINCIDENCE_WINDOW = 1.0
DEFAULT_RATE_ESTIMATION_WINDOW = 1000.0

#-------------------------------------------------

def filter_kw(triggers, start, stop, signif):
    for channel in triggers.keys():
        trgs = triggers[channel]
        trgs = trgs[(start<=trgs['time'])*(trgs['time']<stop)] ### downselect based on coincidence window
        trgs = trgs[signif<=trgs['significance']]                   ### downselect based on significance
        triggers[channel] = trgs
    return triggers

#-------------------------------------------------
 
def analyze(
        frametype,
        start,
        stop,
        target_channel, ### should be KW naming convention
        target_signif=DEFAULT_TARGET_SIGNIFICANCE,
        srate=pointy.DEFAULT_SRATE,
        active_signif=DEFAULT_ACTIVE_SIGNIFICANCE,
        left_coinc_window=DEFAULT_COINCIDENCE_WINDOW,
        right_coinc_window=DEFAULT_COINCIDENCE_WINDOW,
        left_rate_window=DEFAULT_RATE_ESTIMATION_WINDOW,
        right_rate_window=DEFAULT_RATE_ESTIMATION_WINDOW,
        frac_dur=pointy.DEFAULT_FRAC_DUR,
        thresholds=pointy.DEFAULT_THRESHOLDS,
        stride=utils.DEFAULT_KW_STRIDE,
        chanlist_path=None,
        unsafe_chanlist_path=None,
        unsafe_channels_path=None,
        skip_coinc_trigger_generation=False,
        skip_rate_trigger_generation=False,
        output_dir='.',
        tag='',
        verbose=False,
        Verbose=False,
        skip_multi_channel=False,
    ):
    """an end-to-end analysis based on the pointy-poisson statistic
    """
    ### parse some basic options
    verbose |= Verbose
    file_tag = "_"+tag if tag else ""
    dur = stop - start

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    min_threshold = min(thresholds) if (thresholds is not None) else -np.infty
    active_signif = max(active_signif, min_threshold)

    #--------------------

    ### make KW triggers to figure out what is coincident
    coinc_start = start-left_coinc_window
    coinc_stop = stop+right_coinc_window
    coinc_dur = coinc_stop - coinc_start

    coinc_basename = utils.kwbasename(frametype, tag)
    coinc_output_dir = os.path.join(output_dir, coinc_basename)
    if not os.path.exists(coinc_output_dir):
        os.makedirs(coinc_output_dir)

    if chanlist_path is not None:
        if verbose:
            print('extracting chanlist from '+chanlist_path)
        chanlist = utils.path2chanlist(chanlist_path)
    else:
        if verbose:
            print('extracting chanlist from frametype='+frametype)
        chanlist = utils.frametype2chanlist(coinc_start, coinc_stop, frametype) ### remember this for use later
        raw_chanlist_path = os.path.join(output_dir, '%s_chanlist-%d-%d.txt'%(coinc_basename, coinc_start, coinc_dur))
        if verbose:
            print('writing raw chanlist to '+raw_chanlist_path)
        utils.chanlist2path(chanlist, raw_chanlist_path)

    if unsafe_chanlist_path is not None:
        if verbose:
            print('removing unsafe channels (%s) from initial chanlist'%unsafe_chanlist_path)
        unsafe_chanlist = utils.path2chanlist(unsafe_chanlist_path)
        keepers = []
        for tup in chanlist:
            if tup not in unsafe_chanlist:
                keepers.append(tup)
        chanlist = keepers

    # record the initial chanlist used 
    initial_chanlist_path = os.path.join(output_dir, '%s_initial_chanlist-%d-%d.txt'%(coinc_basename, coinc_start, coinc_dur))
    if verbose:
        print('writing initial chanlist to '+initial_chanlist_path)
    utils.chanlist2path(chanlist, initial_chanlist_path)

    inv_chan_map = dict()
    for tup in chanlist:
        for chan in utils.chanlist2channels([tup]):
            inv_chan_map[chan] = tup

    # make sure target channel is in there so I can process it later
    if target_channel not in inv_chan_map:
        raise RuntimeError('target_channel=%s not available in inv_chan_map and therefore will not be produced in rate estimation jobs. This may be resolved by adding the raw channel name to your chanlist_path.')

    if skip_coinc_trigger_generation:
        if verbose:
            print('skipping trigger generation for basic coincidence')
    else:
        if verbose:
            print('generating triggers for basic coincidence within [%.3f, %.3f)'%(coinc_start, coinc_stop))
        utils.makeKWtrgs(
            coinc_start,
            coinc_stop,
            stride,
            frametype,
            chanlist_path=initial_chanlist_path,
            directory=coinc_output_dir,
            tag=tag,
            verbose=Verbose,
        )

    ### find anything that is active within this time period
    if verbose:
        print('reading in triggers to determine coincident channels')
    triggers = utils.paths2KW(
        utils.basename2paths(coinc_start, coinc_stop, coinc_basename, directory=coinc_output_dir, verbose=Verbose),
    )
    triggers = utils.filter_kw(triggers, coinc_start, coinc_stop, active_signif)
    if unsafe_channels_path is not None:
        if verbose:
            print('reading unsafe (kw) channels from '+unsafe_channels_path)
        unsafe_channels = utils.path2channels(unsafe_channels_path)
    else:
        unsafe_channels = []
    active = set([channel for channel, trgs in triggers.items() if len(trgs) and (channel not in unsafe_channels)])

    active_path = os.path.join(output_dir, '%s_active_kw_channels-%d-%d.txt'%(coinc_basename, coinc_start, coinc_dur))
    if verbose:
        print('identified %d coincident channels'%len(active))
        if Verbose:
            for channel in sorted(active):
                print(channel)
        print('writing active channels to: '+active_path)
    utils.channels2path(active, active_path)
    
    #--------------------

    ### generate rate estimation triggers
    rate_start = np.floor(start - left_rate_window)
    rate_stop = np.ceil(stop + right_rate_window)
    rate_dur = rate_stop - rate_start

    rds_tag = 'RDS'+tag
    rds_basename = utils.kwbasename(frametype, rds_tag)
    rds_output_dir = os.path.join(output_dir, rds_basename)
    if not os.path.exists(rds_output_dir):
        os.makedirs(rds_output_dir)

    active.add(target_channel)
    rds_path = os.path.join(output_dir, '%s_rds_chanlist-%d-%d.txt'%(rds_basename, rate_start, rate_dur))
    if verbose:
        print('writing reduced chanlist to '+rds_path)
    utils.chanlist2path(set([inv_chan_map[channel] for channel in active]), rds_path)

    if skip_rate_trigger_generation:
        if verbose:
            print('skipping trigger generation for rate estimation')
    else:
        if verbose:
            print('generating triggers for rate estimation within [%.3f, %.3f)'%(rate_start, rate_stop))    
        utils.makeKWtrgs(
            rate_start,
            rate_stop,
            stride,
            frametype,
            chanlist_path=rds_path,
            directory=rds_output_dir,
            tag=rds_tag,
            verbose=Verbose,
        )

    ### generate timeseries for each channel
    if verbose:
        print('reading in triggers for rate estimation')
    triggers = utils.paths2KW(
        utils.basename2paths(rate_start, rate_stop, rds_basename, directory=rds_output_dir, verbose=Verbose),
        channels=active,
    )
    ### separate off target channel and extrat times for efficiency calculation (below)
    targets = utils.filter_kw({target_channel:triggers.pop(target_channel)}, rate_start, rate_stop, target_signif)[target_channel]['time']

    ### filter for unsafe channels again in case any of them snuck back in through the inv_chan_map logic
    ### for example, if different frequency bands of the (raw) target channel are likely produced but very unsafe
    for channel in unsafe_channels:
        triggers.pop(channel, None) # just remove them from the dict
    ### filter the rest to see what is active
    triggers = utils.filter_kw(triggers, rate_start, rate_stop, min_threshold)

    if verbose:
        print('computing logpvalue_timeseries')
    rate_dur = rate_stop - rate_start
    times = np.arange(rate_start, rate_stop, 1./srate) ### set up time-stamp array
                                                            ### compute pvalue for the entire rate estimation so we can get FAPs
                                                            ### using the rest of the rate estimation as background
    truth = (start<=times)*(times<stop) ### set up this filter to grab only the relevant parts of the timeseries for some later analyses
    timeseries = pointy.logpvalue_timeseries(
        times,
        triggers,
        rate_start,
        rate_stop, 
        window=max(left_coinc_window, right_coinc_window),
        frac_dur=frac_dur,
        thresholds=thresholds,
        verbose=Verbose,
    )
    ### save everything
    timeseries_output_dir = os.path.join(output_dir, 'timeseries%s'%file_tag)
    if not os.path.exists(timeseries_output_dir):
        os.makedirs(timeseries_output_dir)

    ### map data into a slightly different format
    for channel in sorted(timeseries.keys()): ### save these to disk as intermediate products
        lpvl = timeseries[channel]
        timeseries[channel] = {
            'series':lpvl['logpvalue'],
            'path':utils.save(lpvl, channel, rate_start, rate_dur, directory=timeseries_output_dir, tag=file_tag, verbose=Verbose),
        }

    #--------------------

    ### compute statistics
    if verbose:
        print('computing single-channel summary statistics')
    for channel in sorted(timeseries.keys()):
        if Verbose:
            print('processing '+channel)
        series = timeseries[channel]['series']
        val, dtm, eff, N = pointy.timeseries2roc(times, series, targets) ### compute ROC estimate using the full rate window
        timeseries[channel].update({
            'series':series,
            'cdf':{
                'val':val,
                'FAP':dtm,
                'EFF':eff,
                'N':N
            }
        })

    #--------------------

    multi_channel = {}
    if not skip_multi_channel:
        ### compute multi-channel statistics
        ### filter to select which channels we thing are important
        included = {}
        for channel in sorted(timeseries.keys()): ### save these to disk as intermediate products
            lpvl = timeseries[channel]['series']
            logfap = np.log(np.interp(lpvl, timeseries[channel]['cdf']['val'], timeseries[channel]['cdf']['FAP'])) ### use FAP because we know FAP>=0 by construction in the way we calculate it
            logeff = np.log(np.interp(
                lpvl,
                timeseries[channel]['cdf']['val'],
                (timeseries[channel]['cdf']['EFF']*timeseries[channel]['cdf']['N']+1)/(timeseries[channel]['cdf']['N']+2), ### expected value based on counting statistics assuming posterior for EFF is a beta distribution (done so that EFF>0 always and we don't have any single channel kill the product -> logPOST)
            ))
            included[channel] = {'logpvalue':lpvl, 'logFAP':logfap, 'logEFF':logeff, 'logLIKE':logeff-logfap} ### FIXME: actually compute loglikelihood instead of apprxoimating it this way

        ### actually compute the multi-channel statistics
        if verbose:
            print('included %d channels in multi-channel estimation'%len(included))
            print('computing lognaivebayes timeseries')
        lognb = pointy.lognaivebayes([_['logpvalue'] for _ in included.values()]) if len(included) else np.zeros_like(times)
        lognaivebayes_path = utils.save(lognb, 'lognaivebayes', rate_start, rate_dur, directory=timeseries_output_dir, tag=file_tag, verbose=Verbose)
        val, dtm, eff, N = pointy.timeseries2roc(times, lognb, targets)
        multi_channel['lognaivebayes'] = {
            'series':lognb[truth],
            'path':lognaivebayes_path,
            'included':included,
            'cdf':{
                'val':val,
                'FAP':dtm,
                'EFF':eff,
                'N':N
            },
        }

        if verbose:
            print('computing logmin timeseries')
        logmin = pointy.logmin([_['logpvalue'] for _ in included.values()]) if len(included) else np.zeros_like(times)
        logmin_path = utils.save(logmin, 'logmin', rate_start, rate_dur, directory=timeseries_output_dir, tag=file_tag, verbose=Verbose)
        val, dtm, eff, N = pointy.timeseries2roc(times, logmin, targets)
        multi_channel['logmin'] = {
            'series':logmin[truth],
            'path':logmin_path,
            'included':included,
            'cdf':{
                'val':val,
                'FAP':dtm,
                'EFF':eff,
                'N':N
            },
        }

        if verbose:
            print('computing logpost timeseries')
        logPOST = pointy.lognaivebayes([_['logLIKE'] for _ in included.values()]) if len(included) else np.zeros_like(times) ### add the logLIKEs just like we do for naive bayes
        ### transform this by-hand here...
        logPOST = -np.log(1+np.exp(logPOST)) ### we want logPOST ~ log(1/(1+LIKE)) ~ log(prob data is clean) which should have a similar interpreation as our other p-values

        # now save it like the rest
        logPOST_path = utils.save(logPOST, 'logPOST', rate_start, rate_dur, directory=timeseries_output_dir, tag=file_tag, verbose=Verbose)
        val, dtm, eff, N = pointy.timeseries2roc(times, logPOST, targets)
        multi_channel['logPOST'] = {
            'series':logPOST[truth],
            'path':logPOST_path,
            'included':included,
            'cdf':{
                'val':val,
                'FAP':dtm,
                'EFF':eff,
                'N':N
            },
        }

    ### filter out only the data within the requested times
    for channel, val in timeseries.items():
        timeseries[channel]['series'] = val['series'][truth]

    return timeseries, multi_channel, targets[(start<=targets)*(targets<stop)]
