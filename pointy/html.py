from __future__ import absolute_import

__description__ = "a module that houses utility scripts for making html interfaces for pointy-poisson results"
__author__ = "reed.essick@ligo.org"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os
import time
import html

import numpy as np

### non-standard libraries
from . import utils
from . import plots
from . import scans
from . import pointy

try:
    from lal.gpstime import tconvert
except ImportError:
    tconvert = None

#-------------------------------------------------

SOURCE_CODE_URL = 'https://git.ligo.org/reed.essick/pointy-poisson-statistic'

DEFAULT_FAP_THRESHOLDS = [1e-3, 1e-2, 1e-1]

PIPELINE_NAME = 'pointy-pipeline'
SAFETY_NAME = 'pointy-safety-pipeline'

EFF_FAP_NAME = 'efficiency/false alarm probability'
MINFAP_NAME = 'minimum false alarm probability'

CHANNEL_RELATIVE_DIR = 'channels'

LINK_TEMPLATE = '%s (%.3e)'
INT_LINK_TEMPLATE = '%s (%d)'

#-------------------------------------------------

def times2start_dur(times):
    start = times[0]
    dur = len(times)*(times[1]-times[0])
    return start, dur

def _process(foo, danger_thr, warning_thr, *args, **kwargs):
    nargs = len(args)
    danger = [[] for _ in range(nargs)]
    warning = [[] for _ in range(nargs)]
    ok = [[] for _ in range(nargs)]
    for ind, arg in enumerate(args):
        for channel, ans in arg.items():
            val = foo(ans)
            if val <= danger_thr:
                danger[ind].append((channel, val))
            elif val <= warning_thr:
                warning[ind].append((channel, val))
            else:
                ok[ind].append((channel, val))

        danger[ind].sort(key=lambda x:x[1])
        warning[ind].sort(key=lambda x:x[1])
        ok[ind].sort(key=lambda x:x[1])

    no_plot = kwargs.pop('no_plot', False)
    if no_plot: ### used within DQR response
        return danger, warning, ok

    else:
        fig = plots.hist(
            [val for d in danger for _, val in d],
            [val for w in warning for _, val in w],
            [val for o in ok for _, val in o],
            danger_thr=danger_thr,
            warning_thr=warning_thr,
            **kwargs
        )
        return fig, danger, warning, ok

#-------------------------------------------------

def html_path(name, output_dir='.'):
    return os.path.join(output_dir, name+'.html')

def html_head_body(name):
    doc = html.HTML(newlines=True)
    doc.raw_text('<!DOCTYPE html>')
    htmldoc = doc.html(lang='en')
    head = htmldoc.head()

    ### set up header for bootstrap
    head.meta(charset='utf-8')
    head.meta(content='IE=edge')._attrs.update({'http-equiv':"X-UA-Compatible"}) ### forced into modifying _attr by "-" in attribute name
    head.meta(name="viewport", content="width=device-width, initial-scale=1")

    ### other header information
    head.meta(name="description", content=name)
    head.meta(name="author", content=os.getenv('USER', default='unknown')) ### whoever ran this is the author

    ### set up header for this specific template
    head.link(
        href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
        rel="stylesheet",
    )
    head.raw_text('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>')
    head.raw_text('<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>')

    #----------------
    ### build body
    body = htmldoc.body()
    body = body.div(klass='container')

    return doc, head, body

def _summary_intro(
        div,
        title,
        times,
        frametype=None,
        chanlist_path=None,
        channels_path=None,
        target_times_path=None,
        link=None,
        single_channel_links=None,
        multi_channel_links=None,
    ):
    """a helper script that generates the same general layout for both pointy-pipeline and pointy-safety-pipeline
    """
    # add in a banner header
    row = div.div(klass='row')
    col = row.div(klass='col-sm-8')
    col.h1(title)
    col = row.div(klass='col-sm-4')
    if link is not None:
        col.a(link[0], klass='btn btn-link', href=link[1])
    if single_channel_links is not None:
        _summary_dropdown_links(col, 'single channel', single_channel_links)
    if multi_channel_links is not None:
        _summary_dropdown_links(col, 'multi channel', multi_channel_links)

    div.hr()

    # add in start, duration, etc
    start, dur = times2start_dur(times)

    row = div.div(klass='row')
    col = row.div(klass='col-sm-12')
    ul = col.ul()
    if tconvert is None:
        ul.li('start : %.3f'%start)
        ul.li('stop : %.3f'%(start+dur))
        ul.li('duration : %.3f sec'%(dur))
    else:
        ul.li('start : %s (gps=%.3f)'%(tconvert(start), start))
        ul.li('stop : %s (gps=%.3f)'%(tconvert(start+dur), start+dur))
        ul.li('duration : %.3f sec'%(dur))

    ### add links to other configuration params
    col = row.div(klass='col-sm-12')
    ul = col.ul()
    if frametype is not None:
        ul.li('frametype : '+frametype)
    if chanlist_path is not None:
        ul.li().a('raw channel list', href=chanlist_path)
    if channels_path is not None:
        ul.li().a('ETG channel list', href=channels_path)
    if target_times_path is not None:
        ul.li().a('target times', href=target_times_path)

def _summary_dropdown_links(div, title, links, klass='default'):
    d = div.div(klass='btn-group')
    if links:
        d.raw_text('<button class="btn btn-%s dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">%s <span class="caret"></span></button>'%(klass, title))
    
        ul = d.ul(klass='dropdown-menu', style="max-height:1000px; overflow-y:scroll;")
        for name, url in links:
            ul.li().a(name, klass='dropdown-item', href=url)
    else:
        d.raw_text('<button class="btn btn-%s dropdown-toggle disabled" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">%s <span class="caret"></span></button>'%(klass, title))

def _summary_safety_block(
        div,
        name,
        gps_group_path,
        figure_path,
        max_pvalue_warning,
        max_pvalue_danger,
        (num_danger, danger_path, danger_links),
        (num_warning, warning_path, warning_links),
        (num_ok, ok_path, ok_links),
    ):
    row = div.div(klass='row')

    # add header
    col = row.div(klass='col-sm-8')
    col.h2(name)
    col = row.div(klass='col-sm-4')
    col.a('gps times', klass='btn btn-link', href=gps_group_path)

    # add figure
    row = div.div(klass='row')
    row.div(klass='col-sm-12').img(src=figure_path, alt='safety histogram', klass='img-responsive')

    # add table
    table = div.div(klass='row').div(klass='col-sm-12').table(klass='table')

    # unsafe channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.DANGER_LABEL, num_danger), href=danger_path)
    tr.td('logp <= %.3e'%max_pvalue_danger)
    _summary_dropdown_links(tr.td(), 'details', danger_links, klass='danger')

    # suspicious channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.WARNING_LABEL, num_warning), href=warning_path)
    tr.td('%.3e < logp <= %.3e'%(max_pvalue_danger, max_pvalue_warning))
    _summary_dropdown_links(tr.td(), 'details', warning_links, klass='warning')

    # safe channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.OK_LABEL, num_ok), href=ok_path)
    tr.td('%.3e < logp'%(max_pvalue_warning))
    _summary_dropdown_links(tr.td(), 'details', ok_links, klass='success')

def _summary_block(
        div,
        name,
        xlabel,
        figure_path,
        warning_thr,
        danger_thr,
        (single_num_danger, single_danger_path, single_danger_links),
        (single_num_warning, single_warning_path, single_warning_links),
        (single_num_ok, single_ok_path, single_ok_links),
        (multi_num_danger, multi_danger_path, multi_danger_links),
        (multi_num_warning, multi_warning_path, multi_warning_links),
        (multi_num_ok, multi_ok_path, multi_ok_links),
    ):
    danger_text = '%s <= %.3e'%(xlabel, danger_thr)
    warning_text = '%.3e <= %s <= %.3e'%(danger_thr, xlabel, warning_thr)
    ok_text = '%.3e < %s'%(warning_thr, xlabel)

    row = div.div(klass='row')

    ### add header
    col = row.div(klass='col-sm-12')
    col.h2(name)

    ### add figure
    row = col.div(klass='row')
    row.div(klass='col-sm-12').img(src=figure_path, alt='safety histogram', klass='img-responsive')

    ### add tables
    row = col.div(klass='row')
    col = row.div(klass='col-sm-12')

    #--------------------

    ### multi-channel results
    r = col.div(klass='row')
    r.h3('Multi-Channel Results')
    table = r.table(klass='table')

    # danger channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.DANGER_LABEL, multi_num_danger), href=multi_danger_path)
    tr.td(danger_text)
    _summary_dropdown_links(tr.td(), 'details', multi_danger_links, klass='danger')

    # warning channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.WARNING_LABEL, multi_num_warning), href=multi_warning_path)
    tr.td(warning_text)
    _summary_dropdown_links(tr.td(), 'details', multi_warning_links, klass='warning')

    # ok channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.OK_LABEL, multi_num_ok), href=multi_ok_path)
    tr.td(ok_text)
    _summary_dropdown_links(tr.td(), 'details', multi_ok_links, klass='success')

    #--------------------

    ### single-channel results
    r = col.div(klass='row')
    r.h3('Single-Channel Results')
    table = r.table(klass='table')

    # danger channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.DANGER_LABEL, single_num_danger), href=single_danger_path)
    tr.td(danger_text)
    _summary_dropdown_links(tr.td(), 'details', single_danger_links, klass='danger')

    # warning channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.WARNING_LABEL, single_num_warning), href=single_warning_path)
    tr.td(warning_text)
    _summary_dropdown_links(tr.td(), 'details', single_warning_links, klass='warning')

    # ok channels
    tr = table.tr()
    tr.td().a(INT_LINK_TEMPLATE%(plots.OK_LABEL, single_num_ok), href=single_ok_path)
    tr.td(ok_text)
    _summary_dropdown_links(tr.td(), 'details', single_ok_links, klass='success')

def _summary_footer(div):
    row = div.div(klass='row')
    div = row.div(klass='container')
    div.a('pointy-poisson source code', href=SOURCE_CODE_URL)

    div = row.div(klass='container')
    p = div.p('summary generated by')
    ul = p.ul()
    ul.li('username : '+os.getenv('USER', default='unknown'))
    ul.li('hostname : '+os.getenv('HOSTNAME', default='unknown'))
    ul.li('date : '+time.strftime("%H:%M:%S UTC %a %d %b %Y", time.gmtime()))

#------------------------

def timeseries_summary_page(
        name,
        timeseries,
        times,
        cdf=None, ### if specified, use this for ROC curve and background instead of re-computing it directly from timeseries
        output_dir='.',
        toplevel_url=None,
        target_times=None,
        target_times_path=None,
        zoom_window=utils.DEFAULT_CLUSTER_WINDOW,
        cluster_window=utils.DEFAULT_CLUSTER_WINDOW,
        minimum_fap_threshold=utils.DEFAULT_MINIMUM_FAP_THRESHOLD,
        segs_fap_thresholds=DEFAULT_FAP_THRESHOLDS,
        group_window=utils.DEFAULT_CLUSTER_WINDOW,
        omegascan_cnf=None,
        omegascan_exe=scans.DEFAULT_EXE,
        colormap=plots.DEFAULT_COLORMAP,
        scandir='scans', ### if supplied, should be a relative path
        verbose=False,
        force=False,
        skip=False,
    ):
    """make a single-timeseries summary page
    """
    if verbose:
        print('generating html summary page for '+name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # compute
    start, dur = times2start_dur(times)

    if (target_times is not None) and (target_times_path is None):
        target_times_path = 'target_times.txt'
        utils.times2path(target_times, os.path.join(output_dir, target_times_path))
        
    ### make the basic html structure
    doc, head, body = html_head_body(name)

    div = body.div()
    ### fill in the body
    _summary_intro(
        div,
        name,
        times,
        target_times_path=target_times_path,
        link=toplevel_url,
    )

    div.hr()

    # generate timeseries plot and add it
    if verbose:
        print('plotting timeseries')

    div = body.div()

    row = div.div(klass='row')
    col = row.div(klass='col-sm-12')
    col.h2('Timeseries')

    if cdf is None:
        y, cdf_y = pointy.timeseries2cdf(timeseries)
        background = False
    else:
        y = cdf['val']
        cdf_y = cdf['FAP']
        background = True
    curves = {name:(times, timeseries, y, cdf_y)}
    fig = plots.timeseries(
        curves,
        annotate_gps=target_times[(start<=target_times)*(target_times<start+dur)] if target_times is not None else [],
        title=name,
        background=background,
        min_time=start,
        max_time=start+dur,
    )
    figname = os.path.join(output_dir, 'timeseries_%s-%d-%d'%(name, start, dur))
    figname = plots.save(fig, figname, figtypes=['png'])[0]
    plots.close(fig)

    col.img(src=os.path.basename(figname), alt='timeseries', klass='img-responsive')

    # generate ROC, segment lists
    row = div.div(klass='row')

    # make segment lists
    if verbose:
        print('generating segments')
    col = row.div(klass='col-sm-3')
    col.h2('Segments')
    ul = col.ul()
    for threshold in segs_fap_thresholds:
        segs = utils.segments(times, timeseries, np.interp(threshold, cdf_y, y))
        path = utils.savetxt(segs, 'segments', start, dur, directory=output_dir, tag='_%.6f'%threshold)
        if verbose:
            print('wrote '+path)
        ul.li().a('FAP=%.3e (%.3f sec)'%(threshold, utils.livetime(segs)), href=os.path.basename(path))

    # make ROC curve
    if (cdf is not None) or (target_times is not None):
        if verbose:
            print('generating ROC')
        col = row.div(klass='col-sm-9')
        col.h2('Receiver Operating Characteristic')

        if (cdf is not None):
            dt = cdf['FAP']
            eff = cdf['EFF']
            num = cdf['N']
        else:
            dt, eff, num = pointy.timeseries2roc(times, timeseries, target_times)[1:]

        fig = plots.roc({name:(dt, eff, num)}, title=name, legend=False)
        figname = os.path.join(output_dir, 'roc_%s-%d-%d'%(name, start, dur))
        figname = plots.save(fig, figname, figtype=['png'])[0]
        plots.close(fig)

        col.img(src=os.path.basename(figname), alt='ROC', klass='img-responsive')

    # find minima
    if verbose:
        print('finding minima')
    minima = utils.local_minima(
        times,
        timeseries,
        cluster_window=cluster_window,
        threshold=np.interp(minimum_fap_threshold, cdf_y, y),
    )
    minimapath = utils.savetxt(minima, 'minima', start, dur, directory=output_dir)
    if verbose:
        print('wrote '+minimapath)

    row = div.div(klass='row')
    col = row.div(klass='col-sm-12')
    col.h2('Triggers')
    ul = col.ul()
    ul.li.a('minima (FAP <= %.3e)'%minimum_fap_threshold, href=os.path.basename(minimapath))
    ul.li.a('target times', href=target_times_path)

    # generate scans
    if verbose:
        print('clustering minima')

    if target_times is not None:    
        groups = utils.cluster(minima, target_times, window=group_window)
    else:
        groups = utils.cluster(minima, [], window=group_window)
    ntimes = sum(sum(len(_) for _ in x) for x in groups)

    generate_scans = omegascan_cnf is not None
    scan_output_dir = os.path.join(output_dir, scandir)
    if not os.path.exists(scan_output_dir):
        os.makedirs(scan_output_dir)

    if verbose:
        if generate_scans:
            print('generating zoomed timeseries and scans for %d times'%ntimes)
        else:
            print('generating zoomed timeseries for %d times'%ntimes)
    col = row.div(klass='col-sm-12')
    table = col.table(klass='table')
    th = table.thead()
    tr = th.tr()
    tr.th('Minima', scope='col')
    tr.th('Target Times', scope='col')
    tb = table.tbody()

    for m, t in groups:
        tr = tb.tr()
        ### add minima
        _scan_timeseries_row(
            tr.td(),
            name,
            m,
            curves,
            scandir,
            scan_output_dir,
            zoom_window,
            omegascan_exe,
            omegascan_cnf,
            colormap,
            force=force,
            skip=skip,
            verbose=verbose,
        )

        ### add target times
        _scan_timeseries_row(
            tr.td(),
            name,
            t,
            curves,
            scandir,
            scan_output_dir,
            zoom_window,
            omegascan_exe,
            omegascan_cnf,
            colormap,
            force=force,
            skip=skip,
            verbose=verbose,
        )

    # generate footer
    div.hr()
    _summary_footer(body)

    ### write to disk
    path = html_path(name, output_dir=output_dir)
    if verbose:
        print('writing html document to '+path)
    with open(path, 'w') as obj:
        obj.write(str(doc))
    return path

def _scan_timeseries_row(div, name, times, curves, relative_scandir, scan_output_dir, window, exe, cnf, colormap, force=False, skip=False, verbose=False):
    can_scan = cnf is not None

    if not isinstance(times, np.ndarray):
        times = np.array(times)

    for gps in times:
        if verbose:
            print('processing gps=%.3f'%gps)
        r = div.div(klass='row')

        if skip:
            r.div('%.3f'%gps, klass='col-sm-12')

        else:
            start = gps-window
            end = gps+window

            timeseriesbase = 'timeseries_%s-%d-%d'%(name, start, end-start)
            relative_outdir = os.path.join(relative_scandir, '%.6f'%gps)
            outdir = os.path.join(scan_output_dir, '%.6f'%gps)

            if not os.path.exists(outdir):
                os.makedirs(outdir)
                force = True ### we want to make scans et al

            figname = os.path.join(outdir, timeseriesbase)
            if force or (not os.path.exists(figname+'.png')):
                if verbose:
                    print('generating zoomed timeseries')
                # make a timeseries zoom
                fig = plots.timeseries(
                    curves,
                    annotate_gps=times[(start<=times)*(times<end)],
                    min_time=start,
                    max_time=end,
                    background=True,
                )
                plots.save(fig, figname, figtypes=['png'])
                plots.close(fig)

            elif verbose:
                print('skipping zoomed timeseries because file already exists: %s.png'%figname)

            if can_scan:
                if force:
                    scans.scan(gps, exe, cnf, output_dir=outdir, framedir=outdir, colormap=colormap, verbose=verbose)

                elif verbose:
                    print('skipping scan because directory already exists: %s'%outdir)

            if can_scan: ### 3 columns
                r.div('%.3f'%gps, klass='col-sm-4')
                r.div(klass='col-sm-4').a('timeseries', href=os.path.join(relative_outdir, timeseriesbase+'.png'))
                r.div(klass='col-sm-4').a('scan', href=os.path.join(relative_outdir, '%.6f'%gps)) ### match what scans produce

            else: ### 2 columns
                r.div('%.3f'%gps, klass='col-sm-6')
                r.div(klass='col-sm-6').a('timeseries', href=os.path.join(relative_outdir, timeseriesbase+'.png'))

#------------------------

def pipeline_summary_page(
        multi_channel,
        single_channel,
        times,
        target_times=[],
        frametype=None,
        max_eff_fap_danger=pointy.DEFAULT_MAX_EFF_FAP_DANGER,
        max_eff_fap_warning=pointy.DEFAULT_MAX_EFF_FAP_WARNING,
        minfap_danger=pointy.DEFAULT_MINFAP_DANGER,
        minfap_warning=pointy.DEFAULT_MINFAP_WARNING,
        output_dir='.',
        zoom_window=utils.DEFAULT_CLUSTER_WINDOW,
        cluster_window=utils.DEFAULT_CLUSTER_WINDOW,
        minimum_fap_threshold=utils.DEFAULT_MINIMUM_FAP_THRESHOLD,
        segs_fap_thresholds=DEFAULT_FAP_THRESHOLDS,
        group_window=utils.DEFAULT_CLUSTER_WINDOW,
        omegascan_cnf=None,
        omegascan_exe=scans.DEFAULT_EXE,
        colormap=plots.DEFAULT_COLORMAP,
        verbose=False,
        force=False,
        skip=False,
    ):
    """make a pointy-pipeline summary page including links to single-timeseries summary pages
    """
    channel_dir = os.path.join(output_dir, CHANNEL_RELATIVE_DIR)
    for d in [output_dir, channel_dir]:
        if not os.path.exists(d):
            os.makedirs(d)

    ### figure out the link situation 
    final_path = html_path(PIPELINE_NAME, output_dir=output_dir)
    toplevel_url = ('Pointy Summary', os.path.join('..', '..', os.path.basename(final_path))) ### I want a relative path for this link

    ### write channels, target_times, gps_groups to disk
    channels_path = os.path.join(output_dir, 'channels.txt')
    utils.channels2path(sorted(single_channel.keys()), channels_path)

    target_times_path = os.path.join(output_dir, 'target_times.txt')
    utils.times2path(target_times, target_times_path)
    relative_target_times_path = os.path.join('..', '..', os.path.basename(target_times_path))

    ### make individual channel summary pages
    if verbose:
        print('generating individual channel summary pages')
   
    for name, ans in multi_channel.items()+single_channel.items():
        path = timeseries_summary_page(
            name,
            ans['series'],
            times,
            cdf=ans['cdf'],
            output_dir=os.path.join(channel_dir, name),
            toplevel_url=toplevel_url,
            target_times=target_times,
            target_times_path=relative_target_times_path,
            zoom_window=zoom_window,
            cluster_window=cluster_window,
            minimum_fap_threshold=minimum_fap_threshold,
            segs_fap_thresholds=segs_fap_thresholds,
            group_window=group_window,
            omegascan_cnf=omegascan_cnf,
            omegascan_exe=omegascan_exe,
            colormap=colormap,
            verbose=verbose,
            force=force,
            skip=skip,
        )
        ans['html_path'] = os.path.join(CHANNEL_RELATIVE_DIR, name, os.path.basename(path))

    ### format the document
    if verbose:
        print('generating pipeline html summary page')

    doc, head, body = html_head_body(PIPELINE_NAME)

    ### generate the top-level info
    div = body.div()
    _summary_intro(
        div,
        PIPELINE_NAME,
        times,
        frametype=frametype,
        channels_path=os.path.basename(channels_path), ### want relative paths for links
        target_times_path=os.path.basename(target_times_path),
        single_channel_links=[(chan, single_channel[chan]['html_path']) for chan in sorted(single_channel.keys())],
        multi_channel_links=[(chan, multi_channel[chan]['html_path']) for chan in sorted(multi_channel.keys())],
    )

    div.hr()

    ### generate blocks for tabulated summary statistics
    # we generate a histogram over everything for each selection criterion
    # and include separate tables for single-channel and multi-channel statistics
    
    # block for eff/fap threshold
    name='max_eff_fap'
    xlabel='-max(log10(eff/fap))'
    if verbose:
        print('processing statistics for '+xlabel)
    fig, danger, warning, ok = _process( ### FIXME: we should condition on something like the 90% lower limit of eff instead of just the point estimate...
        ( lambda ans: -np.max(np.log10(np.interp(ans['series'], ans['cdf']['val'], ans['cdf']['EFF'])/np.interp(ans['series'], ans['cdf']['val'], ans['cdf']['FAP']))) ),
        -np.log10(max_eff_fap_danger),
        -np.log10(max_eff_fap_warning),
        single_channel,
        multi_channel,
        xlabel=xlabel,
    )
    figure_path = plots.save(fig, os.path.join(output_dir, name), figtypes=['png'], verbose=verbose)[0]
    plots.close(fig)

    single_danger, multi_danger = danger
    single_warning, multi_warning = warning
    single_ok, multi_ok = ok

    # save single-channel files
    single_danger_path = os.path.join(output_dir, 'singlechannel-danger-%s.txt'%name)
    utils.channels2path([channel for channel, _ in single_danger], single_danger_path)
    if verbose:
        print('wrote '+single_danger_path)

    single_warning_path = os.path.join(output_dir, 'singlechannel-suspicious-%s.txt'%name)
    utils.channels2path([channel for channel, _ in single_warning], single_warning_path)
    if verbose:
        print('wrote '+single_warning_path)

    single_ok_path = os.path.join(output_dir, 'singlechannel-safe-%s.txt'%name)
    utils.channels2path([channel for channel, _ in single_ok], single_ok_path)
    if verbose:
        print('wrote '+single_ok_path)

    single_danger_links = [(LINK_TEMPLATE%(channel, val), single_channel[channel]['html_path']) for channel, val in single_danger]
    single_warning_links = [(LINK_TEMPLATE%(channel, val), single_channel[channel]['html_path']) for channel, val in single_warning]
    single_ok_links = [(LINK_TEMPLATE%(channel, val), single_channel[channel]['html_path']) for channel, val in single_ok]

    # save multi-channel files
    multi_danger_path = os.path.join(output_dir, 'multichannel-danger-%s.txt'%name)
    utils.channels2path([channel for channel, _ in multi_danger], multi_danger_path)
    if verbose:
        print('wrote '+multi_danger_path)

    multi_warning_path = os.path.join(output_dir, 'multichannel-suspicious-%s.txt'%name)
    utils.channels2path([channel for channel, _ in multi_warning], multi_warning_path)
    if verbose:
        print('wrote '+multi_warning_path)

    multi_ok_path = os.path.join(output_dir, 'multichannel-safe-%s.txt'%name)
    utils.channels2path([channel for channel, _ in multi_ok], multi_ok_path)
    if verbose:
        print('wrote '+multi_ok_path)

    multi_danger_links = [(LINK_TEMPLATE%(channel, val), multi_channel[channel]['html_path']) for channel, val in multi_danger]
    multi_warning_links = [(LINK_TEMPLATE%(channel, val), multi_channel[channel]['html_path']) for channel, val in multi_warning]
    multi_ok_links = [(LINK_TEMPLATE%(channel, val), multi_channel[channel]['html_path']) for channel, val in multi_ok]

    # format the html
    if verbose:
        print("generating summary block")
    _summary_block(
        div,
        EFF_FAP_NAME,
        xlabel,
        os.path.basename(figure_path),
        -np.log10(max_eff_fap_warning),
        -np.log10(max_eff_fap_danger),
        (len(single_danger), os.path.basename(single_danger_path), single_danger_links),
        (len(single_warning), os.path.basename(single_warning_path), single_warning_links),
        (len(single_ok), os.path.basename(single_ok_path), single_ok_links),
        (len(multi_danger), os.path.basename(multi_danger_path), multi_danger_links),
        (len(multi_warning), os.path.basename(multi_warning_path), multi_warning_links),
        (len(multi_ok), os.path.basename(multi_ok_path), multi_ok_links),
    )

    # block for min(fap) threshold
    name='min_fap'
    xlabel='min(log10(fap))'
    if verbose:
        print('processing statistics for '+xlabel)
    fig, danger, warning, ok = _process(
        ( lambda ans: np.min(np.log10(np.interp(ans['series'], ans['cdf']['val'], ans['cdf']['FAP']))) ),
        np.log10(minfap_danger),
        np.log10(minfap_warning),
        single_channel,
        multi_channel,
        xlabel=xlabel,
    )
    figure_path = plots.save(fig, os.path.join(output_dir, name), figtypes=['png'], verbose=verbose)[0]
    plots.close(fig)

    single_danger, multi_danger = danger
    single_warning, multi_warning = warning
    single_ok, multi_ok = ok

    # save single-channel files
    single_danger_path = os.path.join(output_dir, 'singlechannel-danger-%s.txt'%name)
    utils.channels2path([channel for channel, _ in single_danger], single_danger_path)
    if verbose:
        print('wrote '+single_danger_path)

    single_warning_path = os.path.join(output_dir, 'singlechannel-suspicious-%s.txt'%name)
    utils.channels2path([channel for channel, _ in single_warning], single_warning_path)
    if verbose:
        print('wrote '+single_warning_path)

    single_ok_path = os.path.join(output_dir, 'singlechannel-safe-%s.txt'%name)
    utils.channels2path([channel for channel, _ in single_ok], single_ok_path)
    if verbose:
        print('wrote '+single_ok_path)

    single_danger_links = [(LINK_TEMPLATE%(channel, val), single_channel[channel]['html_path']) for channel, val in single_danger]
    single_warning_links = [(LINK_TEMPLATE%(channel, val), single_channel[channel]['html_path']) for channel, val in single_warning]
    single_ok_links = [(LINK_TEMPLATE%(channel, val), single_channel[channel]['html_path']) for channel, val in single_ok]

    # save multi-channel files
    multi_danger_path = os.path.join(output_dir, 'multichannel-danger-%s.txt'%name)
    utils.channels2path([channel for channel, _ in multi_danger], multi_danger_path)
    if verbose:
        print('wrote '+multi_danger_path)

    multi_warning_path = os.path.join(output_dir, 'multichannel-suspicious-%s.txt'%name)
    utils.channels2path([channel for channel, _ in multi_warning], multi_warning_path)
    if verbose:
        print('wrote '+multi_warning_path)

    multi_ok_path = os.path.join(output_dir, 'multichannel-safe-%s.txt'%name)
    utils.channels2path([channel for channel, _ in multi_ok], multi_ok_path)
    if verbose:
        print('wrote '+multi_ok_path)

    multi_danger_links = [(LINK_TEMPLATE%(channel, val), multi_channel[channel]['html_path']) for channel, val in multi_danger]
    multi_warning_links = [(LINK_TEMPLATE%(channel, val), multi_channel[channel]['html_path']) for channel, val in multi_warning]
    multi_ok_links = [(LINK_TEMPLATE%(channel, val), multi_channel[channel]['html_path']) for channel, val in multi_ok]

    # format the html
    if verbose:
        print("generating summary block")
    _summary_block(
        div,
        MINFAP_NAME,
        xlabel,
        os.path.basename(figure_path),
        np.log10(minfap_warning),
        np.log10(minfap_danger),
        (len(single_danger), os.path.basename(single_danger_path), single_danger_links),
        (len(single_warning), os.path.basename(single_warning_path), single_warning_links),
        (len(single_ok), os.path.basename(single_ok_path), single_ok_links),
        (len(multi_danger), os.path.basename(multi_danger_path), multi_danger_links),
        (len(multi_warning), os.path.basename(multi_warning_path), multi_warning_links),
        (len(multi_ok), os.path.basename(multi_ok_path), multi_ok_links),
    )

    ### add the footer
    div.hr()
    _summary_footer(body)

    ### save the final document
    if verbose:
        print('writing html document to '+final_path)
    with open(final_path, 'w') as obj:
        obj.write(str(doc))
    return final_path

#------------------------

def safety_pipeline_summary_page(
        timeseries,
        times,
        gps_groups,
        target_times=[],
        frametype=None,
        output_dir='.',
        max_pvalue_danger=pointy.DEFAULT_PVALUE_DANGER,
        max_pvalue_warning=pointy.DEFAULT_PVALUE_WARNING,
        zoom_window=utils.DEFAULT_CLUSTER_WINDOW,
        cluster_window=utils.DEFAULT_CLUSTER_WINDOW,
        minimum_fap_threshold=utils.DEFAULT_MINIMUM_FAP_THRESHOLD,
        segs_fap_thresholds=DEFAULT_FAP_THRESHOLDS,
        group_window=utils.DEFAULT_CLUSTER_WINDOW,
        omegascan_cnf=None,
        omegascan_exe=scans.DEFAULT_EXE,
        colormap=plots.DEFAULT_COLORMAP,
        verbose=False,
        force=False,
        skip=False,
    ):
    """make a pointy-safety-pipline summary page including linkns to single-timeseries summary pages
    """
    channel_dir = os.path.join(output_dir, CHANNEL_RELATIVE_DIR)
    for d in [output_dir, channel_dir]:
        if not os.path.exists(d):
            os.makedirs(d)

    ### figure out the link situation 
    final_path = html_path(SAFETY_NAME, output_dir=output_dir)
    toplevel_url = ('Safety Summary', os.path.join('..', '..', os.path.basename(final_path))) ### I want a relative path for this link

    ### write channels, target_times, gps_groups to disk
    channels_path = os.path.join(output_dir, 'channels.txt')
    utils.channels2path(sorted(timeseries.keys()), channels_path)

    target_times_path = os.path.join(output_dir, 'target_times.txt')
    utils.times2path(target_times, target_times_path)
    relative_target_times_path = os.path.join('..', '..', os.path.basename(target_times_path))

    ### make individual channel summary pages
    if verbose:
        print('generating individual channel summary pages')
    for name, ans in timeseries.items():
        path = timeseries_summary_page(
            name,
            ans['series'],
            times,
            cdf=ans['cdf'],
            output_dir=os.path.join(channel_dir, name),
            toplevel_url=toplevel_url,
            target_times=target_times,
            target_times_path=relative_target_times_path,
            zoom_window=zoom_window,
            cluster_window=cluster_window,
            minimum_fap_threshold=minimum_fap_threshold,
            segs_fap_thresholds=segs_fap_thresholds,
            group_window=group_window,
            omegascan_cnf=omegascan_cnf,
            omegascan_exe=omegascan_exe,
            colormap=colormap,
            verbose=verbose,
            force=force,
            skip=skip,
        )
        ans['html_path'] = os.path.join(CHANNEL_RELATIVE_DIR, name, os.path.basename(path))

    ### format the document
    if verbose:
        print('generating safety html summary page')

    doc, head, body = html_head_body(SAFETY_NAME)

    ### generate the top-level info
    div = body.div()
    _summary_intro(
        div,
        SAFETY_NAME,
        times,
        frametype=frametype,
        channels_path=os.path.basename(channels_path), ### want relative paths for links
        target_times_path=os.path.basename(target_times_path),
        single_channel_links=[(chan, timeseries[chan]['html_path']) for chan in sorted(timeseries.keys())],
    )

    div.hr()

    ### generate blocks for tabulated summary statistics
    for name, group in gps_groups.items():
        group_path = os.path.join(output_dir, 'gps_group-%s.txt'%name)
        utils.times2path(group, group_path)

        fig, danger, warning, ok = _process(
            (lambda ans: ans['stacked'][name]),
            np.log(max_pvalue_danger),
            np.log(max_pvalue_warning),
            timeseries,
            title=name,
            xlabel='$\log p$',
        )
        figure_path = plots.save(fig, os.path.join(output_dir, 'gps_group-%s'%name), figtypes=['png'], verbose=verbose)[0]
        plots.close(fig)

        danger = danger[0]
        warning = warning[0]
        ok = ok[0]

        danger_path = os.path.join(output_dir, 'unsafe-%s.txt'%name)
        utils.channels2path([channel for channel, _ in danger], danger_path)

        warning_path = os.path.join(output_dir, 'suspicious-%s.txt'%name)
        utils.channels2path([channel for channel, _ in warning], warning_path)

        ok_path = os.path.join(output_dir, 'safe-%s.txt'%name)
        utils.channels2path([channel for channel, _ in ok], ok_path)

        danger_links = [(LINK_TEMPLATE%(channel, val), timeseries[channel]['html_path']) for channel, val in danger]
        warning_links = [(LINK_TEMPLATE%(channel, val), timeseries[channel]['html_path']) for channel, val in warning]
        ok_links = [(LINK_TEMPLATE%(channel, val), timeseries[channel]['html_path']) for channel, val in ok]

        _summary_safety_block(
            div,
            name,
            os.path.basename(group_path),
            os.path.basename(figure_path),
            np.log(max_pvalue_warning),
            np.log(max_pvalue_danger),
            (len(danger), os.path.basename(danger_path), danger_links),
            (len(warning), os.path.basename(warning_path), warning_links),
            (len(ok), os.path.basename(ok_path), ok_links),
        )

    ### add footer
    div.hr()
    _summary_footer(body)

    ### save the final document
    if verbose:
        print('writing html document to '+final_path)
    with open(final_path, 'w') as obj:
        obj.write(str(doc))
    return final_path
