__description__ = "a module that houses utility scripts for computing pointy-poisson statistics based on streams of triggers"
__author__ = "reed.essick@ligo.org"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import numpy as np
from scipy.stats import gamma as scipy_stats_gamma ### the gamma distribution describes the probability of seeing lognaivebayes

#-------------------------------------------------

DEFAULT_TIME_NAME = "time"
DEFAULT_SIGNIFICANCE_NAME = "significance"
DEFAULT_FREQUENCY_NAME = "frequency"
DEFAULT_DURATION_NAME = "duration"

DEFAULT_THRESHOLDS = None
DEFAULT_FRAC_DUR = 0.01
DEFAULT_WINDOW = 1000
DEFAULT_SRATE = 128.

DEFAULT_PVALUE_DANGER = 1e-3  ### threshold for declaring confidently unsafe
DEFAULT_PVALUE_WARNING = 1e-2 ### threshold for delcaring suspicious

DEFAULT_MAX_EFF_FAP_DANGER = np.infty
DEFAULT_MAX_EFF_FAP_WARNING = 5.

DEFAULT_MINFAP_DANGER = 0.0
DEFAULT_MINFAP_WARNING = 1e-2

#-------------------------------------------------

STATS_DTYPE = [
    ('logpvalue', 'float'),
    ('dt', 'float'),
    ('significance', 'float'),
    ('frequency', 'float'),
    ('duration', 'float'),
    ('rate', 'float'),
]

#-------------------------------------------------

def trgs2thresholds(trgs, significance=DEFAULT_SIGNIFICANCE_NAME):
    return sorted(set(trgs[significance]), reverse=True)

def binKW(trgs, significance=DEFAULT_SIGNIFICANCE_NAME, thresholds=DEFAULT_THRESHOLDS):
    """
    takes a structured array of KW triggers and bins it by thresholds

    assumes thresholds are sorted from biggest to smallest
    """
    if thresholds is None:
        thresholds = trgs2thresholds(trgs, significance=significance)
        
    binned = dict((thr, []) for thr in thresholds) 
    for fields in trgs:
        ### bin data by kwsignif_thr
        for thr in thresholds:
            if fields[significance] >= thr:
                binned[thr].append( fields ) ### add it only to one set 
                break ### do not double count
            else:
                pass ### only remember things that we'll count later on...

    for key, val in binned.items():
        if not val: ### no data, so this threshold is useless
            binned.pop(key)

        elif len(val)==1:
            binned[key] = np.array([val], dtype=trgs.dtype)

        else:
            binned[key] = np.array(val, dtype=trgs.dtype)

    return binned

#------------------------
### single-channel analyses

def logpvalue(
        gps,
        trigs, ### should be a dictionary of numpy structured array
        window=DEFAULT_WINDOW,
        frac_dur=DEFAULT_FRAC_DUR,
        time=DEFAULT_TIME_NAME, 
        significance=DEFAULT_SIGNIFICANCE_NAME,
        frequency=DEFAULT_FREQUENCY_NAME,
        duration=DEFAULT_DURATION_NAME,
        thresholds=DEFAULT_THRESHOLDS,
        verbose=False,
        rate_point_estimate=False,
    ):
    """
    compute the pointy poisson statistic based on a list of triggers.
    does this by minimizing the "p-value" over many different thresholds for a single gps time

    WARNING: assumes user has already filtered the set of triggers to be within a meaningful window, i.e.: we don't filter triggers by time

    FIXME: we may want to choose min_dt dynamically based on each trigger's duration instead of setting a single value...
    """
    window *= 1. ### make sure this is a float
    dur = 2*window
    ans = dict()
    for chan in sorted(trigs.keys()):
        trgs = trigs[chan]
        if verbose:
            print('processing: '+channel)

        if thresholds is None:
            thrs = trgs2thresholds(trgs, significance=significance) ### returns a sorted list
        else:
            thrs = sorted(thresholds, reverse=True)

        logq = 0.

        ### extract useful parameters from triggers
        times = trgs[time]
        signf = trgs[significance]
        freqs = trgs[frequency]
        durs = trgs[duration]
        min_dts = durs*frac_dur

        best_dt = np.nan
        best_signf = np.nan
        best_freq = np.nan
        best_rate = 0.
        best_dur = np.nan
        for threshold in thrs:
            truth = signf >= threshold
            if np.any(truth) or (not rate_point_estimate):
                count = np.sum(truth)
                rate = count / dur

                dt = times[truth]-gps
                min_dt = np.max([np.abs(dt), min_dts[truth]], axis=0) ### FIXME: don't re-allocate here?
                arg = np.argmin(min_dt)
                _dt = min_dt[arg]
                if rate_point_estimate:
                    _logq = np.log(1-np.exp(-rate*2*_dt))         ### NOTE: this uses the point estimate
                else:
                    _logq = np.log(1-(1+2*_dt/dur)**(-(count+1))) - np.log(1 - 2**(-(count+1))) ### NOTE: this marginalizes over unkown rate with a flat prior and accounts for the fact that we truncate at distrib over _dt based on some window (dur)
                if _logq < logq:
                    best_dt = dt[arg] ### record the actual dt, not max(min_dt, dt)
                    best_signf = signf[truth][arg]
                    best_freq = freqs[truth][arg]
                    best_dur = durs[truth][arg]
                    best_rate = rate
                    logq = _logq

        ans[chan] = np.array([(logq, best_dt, best_signf, best_freq, best_dur, best_rate),], dtype=STATS_DTYPE)

    return ans

def logpvalue_timeseries(
        gps,
        trigs, ### should be a dictionary of structured arrays
        rate_estimate_start,
        rate_estimate_stop,
        window=DEFAULT_WINDOW,
        frac_dur=DEFAULT_FRAC_DUR,
        time=DEFAULT_TIME_NAME,    
        significance=DEFAULT_SIGNIFICANCE_NAME,    
        frequency=DEFAULT_FREQUENCY_NAME,    
        duration=DEFAULT_DURATION_NAME,    
        thresholds=DEFAULT_THRESHOLDS,
        verbose=False,
        rate_point_estimate=False,
    ):
    """
    compute the pointy poisson statistic as a timeseries over a list of triggers
    does this by minimizing the "p-value" over many different thresholds for an array of gps times

    FIXME: we may want to choose min_dt dynamically based on each trigger's duration instead of setting a single value...
    """
    ### get boundaries for rate estimation window
    minima = gps-window
    minima[minima<rate_estimate_start] = rate_estimate_start
    maxima = gps+window
    maxima[maxima>rate_estimate_stop] = rate_estimate_stop

    norms = maxima-minima ### normalization for rate estimate
                          ### we divide by this, but should be pretty confident no element==0 based on reasonable input parameters...
    if np.any(norms==0):
        raise ValueError, 'some values of norms==0, so you will want to re-think your command-line arguments'

    ### temporary helper arrays to speed things up by avoiding reallocating memory
    N = len(gps)

    tmp = np.empty((2, N), dtype=float)
    truth = np.empty(N, dtype='bool') ### same here. We're trying hard to avoid re-allocating memory within the loop
    inds = np.arange(N) ### used when setting minimum duration

    lpvl = np.empty(N, dtype=float) ### a holder for log(pval) as a function of time
    counts = np.empty(N, dtype=float) ### these are updated before they're ever used, so we can create empty here
    min_dts = np.empty(N, dtype='float') ### a holder used later

    dts = np.empty(N, dtype='float') ### a holder used later
    rates = np.empty(N, dtype='float')
    signfs = np.empty(N, dtype='float')
    freqs = np.empty(N, dtype='float')
    durs = np.empty(N, dtype='float')

    # compute the biggest windows we'd ever have
    tmp[0,:] = maxima-gps
    tmp[1,:] = gps-minima
    starting_dts = np.max(tmp, axis=0) ### the biggest window we have at each gps time...

    # holders for "best values"
    best_dts = np.empty(N, dtype='float') ### a holder used later
    best_rates = np.empty(N, dtype='float')
    best_signfs = np.empty(N, dtype='float')
    best_freqs = np.empty(N, dtype='float')
    best_durs = np.empty(N, dtype='float')

    ### iterate over thresholds and extremize lnpvl
    ans = dict()
    if thresholds is not None: 
        thresholds.sort(reverse=True) ### sort from biggest to smallest

    for chan in sorted(trigs.keys()):
        trgs = trigs[chan]
        if verbose:
            print('processing: '+chan)

        if thresholds is None:
            thrs = trgs2thresholds(trgs, significance=significance)
        else:
            thrs = thresholds
        binned = binKW(trgs, significance=significance, thresholds=thresholds)

        lpvl[:] = 0.
        counts[:] = 0.

        dts[:] = starting_dts[:]
        min_dts[:] = dts[:]

        best_dts[:] = dts[:]
        best_rates[:] = 0.
        best_signfs[:] = np.nan
        best_freqs[:] = np.nan
        best_durs[:] = np.nan

        for thr in thrs:
            times = binned.get(thr, []) ### default to no triggers if thr is not present (we popped it within binKW)

            if len(times): ### find the minimum dt, otherwise stick with starting_dts
                ### find the mid-points between triggers and then bracket the whole thing with +/-infty
                bounds = [-np.infty] + list(0.5*(times[time][1:]+times[time][:-1])) + [np.infty]

                for m, M, trg in zip(bounds[:-1], bounds[1:], times):
                    t = trg[time]                     ### extract crap
                    counts += (minima<=t)*(t<=maxima) ### add to rate estimate if within the windows

                    ### NOTE: 
                    ###     while we iterate directly over triggers, we only analyze the few gps samples relevant for that trigger instead of the entire array
                    truth[:] = (m<=gps)*(gps<M) ### select off only the times relevant for this triggers
                                                ### at times, neighboring triggers can be so close there are not relevant samples between them...
                    if np.any(truth) or (not rate_point_estimate): ### only perform complicated minimum logic when we have something meaningful to do
                        dts[truth] = gps[truth]-t
                        rates[truth] = counts[truth]/norms[truth]
                        freqs[truth] = trg[frequency]
                        signfs[truth] = trg[significance]
                        durs[truth] = trg[duration]

                        tmp[0,truth] = np.abs(dts[truth])

                        tmp[1,truth] = frac_dur*trg[duration]
                        tmp[0,truth] = np.max(tmp[:,truth], axis=0)

                        tmp[1,truth] = min_dts[truth]
                        min_dts[truth] = np.min(tmp[:,truth], axis=0)

            if not rate_point_estimate:
                truth[:] = counts > 0 ### only update things for which we have a non-zero rate estimate
            else:
                truth[:] = True ### update everything because we are numerically stable against what causes the point estimates to diverge

            tmp[0,truth] = lpvl[truth]
            if rate_point_estimate:
                tmp[1,truth] = np.log(1 - np.exp(-2*min_dts[truth]*counts[truth]/norms[truth]))                                            ### NOTE: this just uses the point estimate
            else:
                tmp[1,truth] = np.log(1 - (1 + 2*min_dts[truth]/norms[truth])**(-(counts[truth]+1))) - np.log(1 - 2**(-(counts[truth]+1))) ### NOTE: this marginalizes over the unknown rate with a flat prior and includes the fact that we truncate the distrib over dt by some window (norms)
            lpvl[truth] = np.min(tmp[:,truth], axis=0) ### minimize at each gps time

            # update the best values
            subset = lpvl[truth] == tmp[1,truth] ### which of the selected times was actually updated
            updated = inds[truth][subset] ### select the indecies that were just updated

            best_dts[updated] = dts[truth][subset]
            best_signfs[updated] = signfs[truth][subset]
            best_freqs[updated] = freqs[truth][subset]
            best_durs[updated] = durs[truth][subset]
            best_rates[updated] = rates[truth][subset]

        ans[chan] = np.array(zip(lpvl, best_dts, best_signfs, best_freqs, best_durs, best_rates), dtype=STATS_DTYPE)

    return ans

#------------------------
### multi-channel analyses

def logmin(lpvls):
    """
    returns the minimum observed pvalue at each time; minimum is taken with respect to different channels
    """
    return np.min(lpvls, axis=0)

def logpdflogmin(logm, N):
    """
    compute the probability distribiton of seeing logm given N channels
    """
    m = np.exp(logm)
    return logm + np.log(N) + (N-1)*np.log(1-np.exp(logm))

def logcdfmin(logm, N):
    """
    compute the cumulative probability of observing the minimum p-value where we did given the number of channels used
    assumes all channels are i.i.d. and p-values fed in are drawn from ~ U[0,1]

    the probability of the minimum being where it was observed is the probability of seeing only results larger than that.
    for each channel, this is "1-pvalue", and we assume all channels are independent to the total probability is

        P(min < X) = 1 - (1 - X)**Nchannels
    """
    return np.log(1 - (1-np.exp(logm))**N)

#---

def lognaivebayes(lpvls):
    """
    compute the naive bayes joint probability of observing the set of p-values we did
    assumes all channels are independent

    the joint probability is just the product of the individual probabilities, so we just sum the logpvalues across channels
    """
    return np.sum(lpvls, axis=0)

def logpdflognaivebayes(lognb, N):
    """
    return the probability distribution function of seeing lognaivebayes=lognb with N channels
    """
    return scipy_stats_gamma.logpdf(-lognb, N+1)

def logcdfnaivebayes(lognb, N):
    """
    compute the cumulative probability of the naive bayes statistic. This is expected to be Gamma distributed as follows

      p(lognb) ~ exp(lognb) * (-lognb)**N / Gamma(N+1)

    where N = len(lpvls). We return the cumulative distribution, for which lognb <= observed value

    Assumes all channels are i.i.d. and p-values fed in are drawn from ~U[0,1]
    """
    return scipy_stats_gamma.logsf(-lognb, N+1) ### log of survival function, which is log(1-cdf).
                                                ### This is the integral we actually want as it correponds to lognb <= observed
                                                ### NOTE: this may return -np.infty at times

#------------------------
### timeseries summary information

def timeseries2cdf(lpvl, thresholds=None):
    """return y, cdf(y)
    """
    if thresholds is None:
        thresholds = np.array(sorted(set(list(lpvl))), dtype=float)
    else:
        thresholds = sorted(thresholds)
    cdf = np.empty_like(thresholds, dtype=float)
    N = len(lpvl)
    for i, thr in enumerate(thresholds):
        cdf[i] = 1.*np.sum(lpvl<=thr)/N
    return thresholds, cdf

def timeseries2roc(gps, lpvl, target_times):
    """return deadtime, eff
    """
    Ntarget = len(target_times)
    Ngps = len(gps)
    deadtime = []
    eff = []
#    thresholds = [np.min(lpvl)]+sorted(np.interp(target_times, gps, lpvl)) ### only where we have glitch samples
    thresholds = sorted(set(list(lpvl)))
    vect = np.interp(target_times, gps, lpvl)
    for i, thr in enumerate(thresholds):
        deadtime.append( 1.*np.sum(lpvl<=thr)/Ngps )
        if Ntarget>0:
#            eff.append(1.*i/Ntarget)
             eff.append( 1.*np.sum(vect<=thr)/Ntarget )
        else:
            eff.append(0) ### handle this as a special case...
    thresholds.append(np.max(lpvl))
    deadtime.append(1)
    eff.append(1)
    return np.array(thresholds, dtype=float), np.array(deadtime, dtype=float), np.array(eff, dtype=float), Ntarget
