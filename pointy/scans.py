__description__ = "a module that houses utilities for making omegascans"
__author__ = "reed.essick@ligo.org"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os
import subprocess as sp

### non-standard libraries
from . import utils
from . import plots

#-------------------------------------------------

DEFAULT_EXE = '/home/omega/opt/omega/bin/wpipeline'
COMMAND_TEMPLATE = "%(omegascan_exe)s scan %(gps).6f -c %(omegascan_cnf)s -f %(framedir)s -o %(output_dir)s/%(gps).6f -r -m %(colormap)s"

#-------------------------------------------------

def cnf2frametypes(path):
    '''
    parses an OmegaScan config file, returning a list of (frametype, window) required by that config

    WARNING: key values used here are hard coded based on what OmegaScans use, which may be fragile...
    '''
    return set([(chan['frameType'], max(chan['searchTimeRange'], *chan['plotTimeRanges'])) for chan in parse_omegascan_cnf(path)])

def parse_omegascan_cnf(path):
    '''
    parses OmegaScan config file into a list of dictionaries.
    '''
    chans = []
    with open(path, 'r') as file_obj:
        for line in file_obj:
            line = line.strip()

            if line and (line[0] == "{"): ### beginning of channel declaration
                chan = dict()

                line = file_obj.next().strip()
                while not line.endswith('}'): ### go until we hit the closing bracket
                    key, val = line.split(':', 1) ### at most one split
                    chan[key.strip()] = utils.str2obj(val) ### assign parsed string to key

                    line = file_obj.next().strip() ### bump iteration

                chans.append( chan )
    return chans

OMEGASCAN_SECTION_TEMPLATE = '[%(section)s,%(section)s]'
OMEGASCAN_CHANNEL_TEMPLATE = '''\
{
  channelName:               '%(channelName)s'
  frameType:                 '%(frameType)s'
  sampleFrequency:           %(sampleFrequency).6f
  searchTimeRange:           %(searchTimeRange).6f
  searchFrequencyRange:      [%(searchFrequencyRange)s]
  searchQRange:              [%(searchQRange)s]
  searchMaximumEnergyLoss:   %(searchMaximumEnergyLoss).6f
  whiteNoiseFalseRate:       %(whiteNoiseFalseRate).6e
  searchWindowDuration:      %(searchWindowDuration).6f
  plotTimeRanges:            [%(plotTimeRanges)s]
  plotFrequencyRange:        [%(plotFrequencyRange)s]
  plotNormalizedEnergyRange: [%(plotNormalEnergyRange)s]
  alwaysPlotFlag:            %(alwaysPlotFlag)d
}
'''
OMEGASCAN_SPECIAL_KEYS = ['searchFrequencyRange', 'searchQRange', 'plotTimeRanges', 'plotNormalizedEnergyRange']
def write_omegascan_cnf(sections, path):
    """writes OmegaScan config file based on sections into path
    sections = {'name':[chan_dict, chan_dict,...], ...}
    """
    with open(path, 'w') as obj:
        for section, chans in sections.items():
            print >> obj, OMEGASCAN_SECTION_TEMPLATE%{'section':section}
            for chan in chans:
                chan = dict(chan) ### make a copy so I can muck with it
                for key in OMEGASCAN_SPECIAL_KEYS:
                    chan[key] = ' '.join('%.6f'%_ for _ in chan.get(key, []))
                print >> obj, OMEGASCAN_CHANNEL_TEMPLATE%chan

#-------------------------------------------------

def scan(gps, omegascan_exe, omegascan_cnf, frametypes=None, output_dir='.', framedir='.', colormap=plots.DEFAULT_COLORMAP, verbose=False, launch=True):
    ### make local copies of all necessary frames
    if verbose:
        print('making local references to frames for gps=%.3f'%gps)
    if frametypes is None:
        frametypes = cnf2frametypes(omegascan_cnf)

    for frametype, window in frametypes:
        for frame in utils.frametype2inlist(gps-window, gps+window, 0, frametype).strip().split():
            newpath = os.path.join(framedir, os.path.basename(frame))
            if verbose:
                print('    %s -> %s'%(frame, newpath))
            if not os.path.exists(newpath):
                sp.Popen(['ln', '-s', frame, newpath]).wait() ### make a soft-link to the frame

    ### format command, add to list
    cmd = COMMAND_TEMPLATE%{\
        'omegascan_exe':omegascan_exe,
        'omegascan_cnf':omegascan_cnf,
        'output_dir':output_dir,
        'framedir':framedir,
        'colormap':colormap,
        'gps':gps,
    }
    if verbose:
        print('    '+cmd)

    if launch:
        proc = sp.Popen(cmd.split(), stdout=sp.PIPE, stderr=sp.PIPE)
        stdout, stderr = proc.communicate()
        if proc.returncode:
            raise RuntimeError('scan exited with returncode=%d\n%s\nSTDOUT: %s\nSTDERR: %s'%(proc.returncode, cmd, stdout, stderr))
