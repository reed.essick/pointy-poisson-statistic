__description__ = "a module that houses utility scripts for plotting pointy-poisson statistics"
__author__ = "reed.essick@ligo.org"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import numpy as np
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
plt.rcParams['text.usetex'] = True

from . import pointy

#-------------------------------------------------

DEFAULT_FIGTYPES = ['png']

DEFAULT_TIMESERIES_FIGWIDTH = 10
DEFAULT_TIMESERIES_FIGHEIGHT = 5

AXT_POS = [0.10, 0.12, 0.50, 0.80]
AXC_POS = [0.61, 0.12, 0.30, 0.80]

DEFAULT_ALPHA = 0.75

DEFAULT_REFERENCE_TIME = None
DEFAULT_XMIN = DEFAULT_XMAX = DEFAULT_YMIN = None

DEFAULT_COLORMAP = 'parula'

#-------------------------------------------------

def save(figure, basename, figtypes=DEFAULT_FIGTYPES, verbose=False, **kwargs):
    paths = []
    template = basename+".%s"
    for figtype in figtypes:
        figname = template%figtype
        if verbose:
            print('saving: '+figname)
        figure.savefig(figname, **kwargs)
        paths.append(figname)
    return paths

def close(figure):
    plt.close(figure)

#-------------------------------------------------

def logmin(logmin, num_lpvls, title=None):
    fig = plt.figure()
    ax = fig.gca()

    truth = logmin > -np.infty
    bins = np.linspace(np.min(logmin[truth]), np.max(logmin[truth]), max(10, int(len(logmin)**0.5)))
    theory = np.linspace(bins[0], bins[-1], 1001) ### values for theory predicitons

    ax.hist(logmin, bins=bins, histtype='step', label='observed', normed=True, log=True)
    ax.plot(theory, np.exp(pointy.logpdflogmin(theory, num_lpvls)), label='theory')

    ax.set_xlim(xmin=bins[0], xmax=bins[-1])

    ax.set_xlabel('$\log p_\mathrm{min}$')
    ax.set_ylabel('PDF')

    ax.grid(True, which='both')
    ax.legend(loc='best')

    if title is not None:
        fig.suptitle(title.replace('_','\_'))

    return fig

def lognb(lognb, num_lpvls, title=None):
    fig = plt.figure()
    ax = fig.gca()
    
    truth = lognb > -np.infty
    bins = np.linspace(np.min(lognb[truth]), np.max(lognb[truth]), max(10, int(len(lognb)**0.5)))
    theory = np.linspace(bins[0], bins[-1], 1001) ### values for theory predicitons

    ax.hist(lognb, bins=bins, histtype='step', label='observed', normed=True, log=True)
    ax.plot(theory, np.exp(pointy.logpdflognaivebayes(theory, num_lpvls)), label='theory')
    
    ax.set_xlim(xmin=bins[0], xmax=bins[-1])
    
    ax.set_xlabel('$\log p_\mathrm{nb}$')
    ax.set_ylabel('PDF')
    
    ax.grid(True, which='both')
    ax.legend(loc='best')

    if title is not None:
        fig.suptitle(title.replace('_','\_'))

    return fig

def roc(
        curves, # {label:(deadtime, efficiency, num_targets), ...}
        verbose=False,
        alpha=DEFAULT_ALPHA,
        title=None,
        legend=True,
        marker=None,
    ):
    fig = plt.figure()
    ax = fig.gca()

    xmin = 1e-4
    for label, (dtm, eff, N) in curves.items():
        if verbose:
            print('plotting: '+label)
        color = ax.plot(dtm, eff, marker=marker, label=label, alpha=alpha)[0].get_color()
        s = (eff*(1-eff)/N)**0.5
        ax.fill_between(dtm, eff+s, eff-s, alpha=alpha/2., color=color)

        xmin = min(xmin, min(dtm[dtm>0])/1.1)

    fair_coin = np.logspace(np.log10(xmin), 0, 1001)
    ax.plot(fair_coin, fair_coin, color='k', linestyle='--', alpha=0.50)

    ax.set_xlabel('Fractinal Deadtime')
    ax.set_ylabel('Veto Efficiency')

    ax.set_yscale('linear')
    ax.set_ylim(ymin=0, ymax=1)

    ax.set_xscale('log')
    ax.set_xlim(xmin=xmin, xmax=1)

    ax.grid(True, which='both')
    if legend:
        ax.legend(loc='best')

    if title is not None:
        fig.suptitle(title.replace('_','\_'))

    return fig

def timeseries(
        curves, # {label:(x, y, bkgnd_y, bkgnd_cdf(bkgnd_y)), ...}
        annotate_gps=[],
        alpha=DEFAULT_ALPHA,
        figwidth=DEFAULT_TIMESERIES_FIGWIDTH,
        figheight=DEFAULT_TIMESERIES_FIGHEIGHT,
        reference_time=DEFAULT_REFERENCE_TIME,
        min_time=DEFAULT_XMIN,
        max_time=DEFAULT_XMAX,
        ymin=DEFAULT_YMIN,
        title=None,
        rescale=False,
        onsource=True,
        background=True,
        legend=False,
        verbose=False,
    ):

    ### set up axes
    fig = plt.figure(figsize=(figwidth, figheight))

    ax_t = fig.add_axes(AXT_POS) # timeseries
    ax_c = fig.add_axes(AXC_POS) # cumulative histograms

    axs = [ax_t, ax_c]

    if reference_time is None:
        if min_time is not None:
            reference_time = min_time
        elif max_time is not None:
            reference_time = max_time

    ### iterate over args, plotting each in turn
    xmin = +np.infty
    xmax = -np.infty
    callouts = []
    for label, (x, y, cdf_y, cdf) in curves.items():
        if verbose:
            print('plotting: '+label)

        ### filter data as needed
        truth = np.ones_like(y, dtype=bool)
        if min_time is not None:
            truth *= min_time <= x
        if max_time is not None:
            truth *= x < max_time

        N = np.sum(truth)
        if not N: ### nothing to plot! skip this data
            continue

        x = x[truth]
        y = y[truth]

        # figure out limits
        xmin = min(x[0], xmin)
        xmax = max(x[-1], xmax)

        if reference_time is None:
            time = x
        else:
            time = x - reference_time

        if ymin is not None:
            y[y==-np.infty] = ymin ### map these values to the minimum yvalue allowed
        else:
            y[y==-np.infty] = np.min(y[y>-np.infty])

        if rescale: ### rescale x so it has unit variance
            s = np.std(y)
            if s > 0: # need to handle this case specially, because sometimes the time-series are flat (x=0 for all times)
                y /= s
                cdf_y /= s

        label = label.replace('_','\_')

        color = ax_t.plot(time, y, label=label, alpha=alpha)[0].get_color()

        if onsource:
            ax_c.hist( # plot the observed distrib in the specified (smaller) range
                y[(-np.infty<y)*(y<+np.infty)], ### get rid of infinite values, which happen occasionally
                bins=min(1000, 10*N),
                histtype='step',
                label=label+' on-source',
                color=color,
                alpha=alpha,
                normed=True,
                log=True,
                cumulative=True,
                orientation='horizontal',
            )
            linestyle = 'dashed'

        else:
            linestyle = 'solid'

        if background:
            ax_c.plot(cdf, cdf_y, color=color, alpha=alpha, linestyle=linestyle, label=label+' background') ### plot the expected distrib
            ax_c.set_xscale('log')

        for GPS in annotate_gps:
            callouts.append( (color, GPS, np.interp(GPS, x, y)) )

    # override the min and max times if specified
    if min_time is not None:
        xmin = min_time
    if max_time is not None:
        xmax = max_time

    ### decorate
    if reference_time is None:
        ax_t.set_xlabel('Time [sec]')
        ax_t.set_xlim(xmin=xmin, xmax=xmax)
    else:
        ax_t.set_xlabel('Time relative to %.3f [sec]'%reference_time)
        ax_t.set_xlim(xmin=xmin-reference_time, xmax=xmax-reference_time)

    if rescale:
        ax_t.set_ylabel('$(\log p)/\sigma_{\log p}$')
    else:
        ax_t.set_ylabel('$\log p$')

    ax_c.set_xlabel('CDF')
    plt.setp(ax_c.get_yticklabels(), visible=False)

    min_y = np.infty
    max_y = -np.infty
    for ax in axs:
        m, M = ax.get_ylim()
        min_y = min(m, min_y)
        max_y = max(M, max_y)
    max_y = min(max_y, 0) ### we're plotting logpvalue, so this must be negative...
    if ymin is not None:
        min_y = ymin ### override if specified

    for ax in axs:
        ax.set_ylim(ymin=min_y, ymax=max_y)
        ax.grid(True, which='both')

    # add annotation callouts
    t_xlim = ax_t.get_xlim()
    c_xlim = ax_c.get_xlim()
    for color, GPS, y in callouts:
        if reference_time is not None:
            GPS -= reference_time
        ax_t.plot([GPS]*2, [min_y, max_y], color=color, linestyle='solid', alpha=alpha/2.)
        ax_t.plot(t_xlim, [y]*2, color=color, linestyle='solid', alpha=alpha/2.)
        ax_c.plot(c_xlim, [y]*2, color=color, linestyle='solid', alpha=alpha/2.)

    ax_t.set_xlim(t_xlim)
    ax_c.yaxis.tick_right()
    ax_c.yaxis.set_label_position('right')
    ax_c.set_xlim(c_xlim)

    if title is not None:
        fig.suptitle(title.replace('_','\_'))

    if legend:
        ax_c.legend(loc='lower right')

    return fig

OK_COLOR = 'g'
OK_LABEL = 'ok'
WARNING_COLOR = 'b'
WARNING_LABEL = 'warning'
DANGER_COLOR = 'r'
DANGER_LABEL = 'danger'
def hist(danger, warning, ok, title=None, xlabel=None, legend=True, danger_thr=None, warning_thr=None, num_bins=None):
    fig = plt.figure()
    ax = fig.gca()

    if num_bins is None:
        num_bins = max(10, round((len(danger)+len(warning)+len(ok))**0.5, 0))
    _, bins = np.histogram(list(danger)+list(warning)+list(ok), num_bins) ### figure out a consistent binning
    bins = np.exp(bins)
    for samples, color, label in [(danger, DANGER_COLOR, DANGER_LABEL), (warning, WARNING_COLOR, WARNING_LABEL), (ok, OK_COLOR, OK_LABEL)]:
        if len(samples):
            ax.hist(np.exp(samples), bins=bins, color=color, histtype='step', label=label)

    ax.set_yscale('log')
    ax.set_xscale('log')

    ax.set_xlim(xmax=1.)

    ax.set_ylabel('count')
    if xlabel is not None:
        ax.set_xlabel(xlabel)

    ylim = ax.get_ylim()
    if ylim[0]==1:
        ylim = (0.9, ylim[1])
    if danger_thr is not None:
        ax.plot([np.exp(danger_thr)]*2, ylim, color=DANGER_COLOR, alpha=0.5, linestyle='dashed')
    if warning_thr is not None:
        ax.plot([np.exp(warning_thr)]*2, ylim, color=WARNING_COLOR, alpha=0.5, linestyle='dashed')
    ax.set_ylim(ylim)

    if title is not None:
        fig.suptitle(title.replace('_','\_'))

    if legend:
        ax.legend(loc='best')

    return fig
